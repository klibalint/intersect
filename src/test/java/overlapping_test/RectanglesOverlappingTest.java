package overlapping_test;

import org.junit.Test;
import overlapping.RectanglesOverlapping;
import plane_figures.Rectangle;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class RectanglesOverlappingTest {

    @Test
    public void testIs2RectanglesOverlappingByFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 5, 5);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs2RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, 10, 200, 300);
        Rectangle r2 = new Rectangle(1, -3, 20, 30);
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs2RectanglesOverlappingByFullOverlappingAndMinusCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 5, 5);
        Rectangle r2 = new Rectangle(-6, -6, 5, 5);
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs2RectanglesOverlappingByPartOverlapping() {
        Rectangle r1 = new Rectangle(1, 1, 2, 2);
        Rectangle r2 = new Rectangle(2, 2, 2, 2);
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs2RectanglesOverlappingByPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-40, 0, 234, 168);
        Rectangle r2 = new Rectangle(23, -2, 2234, 324552);
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs2RectanglesOverlappingByPartOverlappingAndMinusCoordinate() {
        Rectangle r1 = new Rectangle(-404, -3220, 10000, 900);
        Rectangle r2 = new Rectangle(-405, -3221, 5, 5);
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertTrue(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs2RectanglesOverlappingByNoOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 4, 4);
        Rectangle r2 = new Rectangle(4, 4, 4, 4);
        assertFalse(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertFalse(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs2RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-23, 25, 23, 25);
        Rectangle r2 = new Rectangle(44, -18, 231, 525);
        assertFalse(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertFalse(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs2RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-23, -25, 23, 25);
        Rectangle r2 = new Rectangle(-25, -27, 2, 2);
        assertFalse(RectanglesOverlapping.is2RectanglesOverlapping(r1, r2));
        assertFalse(RectanglesOverlapping.is2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testIs3RectanglesOverlappingByFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 5, 5);
        Rectangle r2 = new Rectangle(0, 0, 4, 4);
        Rectangle r3 = new Rectangle(0, 0, 3, 3);
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs3RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 20, 20);
        Rectangle r2 = new Rectangle(5, -5, 5, 10);
        Rectangle r3 = new Rectangle(-1, 1, 11, 2);
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs3RectanglesOverlappingByFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-20, -20, 19, 19);
        Rectangle r2 = new Rectangle(-15, -15, 14, 14);
        Rectangle r3 = new Rectangle(-5, -5, 2, 2);
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs3RectanglesOverlappingBy3RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(1, 1, 40, 40);
        Rectangle r2 = new Rectangle(0, 1, 6, 8);
        Rectangle r3 = new Rectangle(0, 0, 3, 2);
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs3RectanglesOverlappingBy3RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, 25, 100, 200);
        Rectangle r2 = new Rectangle(0, -50, 100, 200);
        Rectangle r3 = new Rectangle(-5, -25, 100, 200);
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs3RectanglesOverlappingBy3RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 20);
        Rectangle r2 = new Rectangle(-10, -10, 6, 6);
        Rectangle r3 = new Rectangle(-5, -6, 3, 2);
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertTrue(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs3RectanglesOverlappingBy2RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 20);
        Rectangle r2 = new Rectangle(2, 2, 6, 6);
        Rectangle r3 = new Rectangle(11, 21, 343, 222);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
    }

    @Test
    public void testIs3RectanglesOverlappingBy2RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 20);
        Rectangle r2 = new Rectangle(-1, 1, 6, 6);
        Rectangle r3 = new Rectangle(123, -21, 343, 222);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
    }

    @Test
    public void testIs3RectanglesOverlappingBy2RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 20);
        Rectangle r2 = new Rectangle(-1, -1, 6, 6);
        Rectangle r3 = new Rectangle(-123, -21, 8, 7);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
    }

    @Test
    public void testIs3RectanglesOverlappingBy2RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(4, 4, 66, 56);
        Rectangle r2 = new Rectangle(3, 3, 2, 2);
        Rectangle r3 = new Rectangle(1, 1, 1, 1);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
    }

    @Test
    public void testIs3RectanglesOverlappingBy2RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-4, 4, 14, 26);
        Rectangle r2 = new Rectangle(3, -23, 2, 2987);
        Rectangle r3 = new Rectangle(-100, 1, 12, 11);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
    }

    @Test
    public void testIs3RectanglesOverlappingBy2RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-40, -40, 40, 40);
        Rectangle r2 = new Rectangle(-50, -50, 20, 22);
        Rectangle r3 = new Rectangle(-100, -120, 7, 8);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
    }

    @Test
    public void testIs3RectanglesOverlappingByNoOverlapping() {
        Rectangle r1 = new Rectangle(10, 10, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(20, 20, 4300, 2310);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs3RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-247, 3487, 17, 13);
        Rectangle r2 = new Rectangle(4563, -3487, 444, 44);
        Rectangle r3 = new Rectangle(-2474, 16000, 342, 234);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs3RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1000, -10000, 435, 4350);
        Rectangle r2 = new Rectangle(-1, -1, 2, 2);
        Rectangle r3 = new Rectangle(-400, -4000, 43, 435);
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r2, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r1, r3, r2));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r1, r3));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r2, r3, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r2, r1));
        assertFalse(RectanglesOverlapping.is3RectanglesOverlapping(r3, r1, r2));
    }

    @Test
    public void testIs4RectanglesOverlappingBy4RectsFullOverlapping() {
        //full overlapping
        Rectangle r1 = new Rectangle(1, 1, 100, 100);
        Rectangle r2 = new Rectangle(2, 2, 99, 99);
        Rectangle r3 = new Rectangle(3, 3, 98, 98);
        Rectangle r4 = new Rectangle(4, 4, 97, 97);
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r3, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r3, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingBy4RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-25, 25, 30, 75);
        Rectangle r2 = new Rectangle(-20, 30, 25, 65);
        Rectangle r3 = new Rectangle(-15, 35, 20, 60);
        Rectangle r4 = new Rectangle(-10, 40, 15, 55);

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r3, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r3, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingBy4RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-200, -200, 100, 100);
        Rectangle r2 = new Rectangle(-175, -175, 75, 75);
        Rectangle r3 = new Rectangle(-150, -150, 50, 50);
        Rectangle r4 = new Rectangle(-125, -125, 25, 25);

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r3, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r3, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingBy4RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 1500, 2000);
        Rectangle r2 = new Rectangle(1000, 1000, 1500, 2000);
        Rectangle r3 = new Rectangle(1400, 1400, 35500, 72000);
        Rectangle r4 = new Rectangle(1300, 1300, 15009, 267000);

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r3, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r3, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r1, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingBy4RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(200, -300, 800, 1300);
        Rectangle r2 = new Rectangle(-200, 300, 500, 13000);
        Rectangle r3 = new Rectangle(150, -400, 700, 43567);
        Rectangle r4 = new Rectangle(-10, -10, 1000, 1300);

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r3, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r3, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingBy4RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1000, -1000, 500, 500);
        Rectangle r2 = new Rectangle(-1100, -1100, 500, 500);
        Rectangle r3 = new Rectangle(-1200, -1200, 500, 500);
        Rectangle r4 = new Rectangle(-1300, -1300, 500, 500);

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));

        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r3, r1));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertTrue(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingBy2RectsOverlapping() {
        Rectangle r1 = new Rectangle(-1000, -1000, 1500, 1500);
        Rectangle r2 = new Rectangle(-1, -1, 50, 50);
        Rectangle r3 = new Rectangle(-12001, -14200, 500, 500);
        Rectangle r4 = new Rectangle(81300, 781300, 500, 500);

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r3, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingBy3RectsOverlapping() {
        Rectangle r1 = new Rectangle(-1000, -1000, 1500, 1500);
        Rectangle r2 = new Rectangle(-1, -1, 50, 50);
        Rectangle r3 = new Rectangle(-500, -400, 500, 500);
        Rectangle r4 = new Rectangle(81300, 781300, 500, 500);

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingByNoOverlapping() {
        Rectangle r1 = new Rectangle(10, 10, 10, 10);
        Rectangle r2 = new Rectangle(20, 20, 10, 10);
        Rectangle r3 = new Rectangle(30, 30, 10, 10);
        Rectangle r4 = new Rectangle(40, 40, 10, 10);

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r4, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r3, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r1, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r3, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r1, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, 20, 2, 3);
        Rectangle r2 = new Rectangle(10, -3400, 232, 280);
        Rectangle r3 = new Rectangle(-80, 40, 32, 1);
        Rectangle r4 = new Rectangle(-11, 21, 1, 1);

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r4));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r3, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r1, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r3, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r1, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r2, r1));
    }

    @Test
    public void testIs4RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-200, -400, 20, 40);
        Rectangle r2 = new Rectangle(-200, -400, 20, 40);
        Rectangle r3 = new Rectangle(-200, -400, 200, 400);
        Rectangle r4 = new Rectangle(-20000, -40000, 2000, 4000);

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r2, r4, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r3, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r2, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r1, r4, r3, r2));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r1, r3, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r3, r4, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r1, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r2, r4, r3, r1));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r2, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r1, r4, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r1, r4));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r2, r4, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r1, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r3, r4, r2, r1));

        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r1, r3, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r1, r3));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r2, r2, r1));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r1, r2));
        assertFalse(RectanglesOverlapping.is4RectanglesOverlapping(r4, r3, r2, r1));
    }

    @Test
    public void testIs5RectanglesOverlappingByFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(2, 2, 6, 6);
        Rectangle r4 = new Rectangle(3, 3, 4, 4);
        Rectangle r5 = new Rectangle(4, 4, 2, 2);

        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));
    }

    @Test
    public void testIs5RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, 10, 20, 20);
        Rectangle r2 = new Rectangle(-8, 12, 22, 20);
        Rectangle r3 = new Rectangle(-6, 14, 24, 63);
        Rectangle r4 = new Rectangle(-4, 16, 26, 9);
        Rectangle r5 = new Rectangle(-2, 18, 28, 22);

        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));
    }

    @Test
    public void testIs5RectanglesOverlappingByFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(-2, -2, 8, 8);
        Rectangle r3 = new Rectangle(-1, -1, 21, 63);
        Rectangle r4 = new Rectangle(-3, -3, 3, 3);
        Rectangle r5 = new Rectangle(-4, -4, 32, 22);

        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));
    }

    @Test
    public void testIs5RectanglesOverlappingByPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(5, 5, 8, 8);
        Rectangle r3 = new Rectangle(6, 6, 21, 63);
        Rectangle r4 = new Rectangle(5, 6, 3, 3);
        Rectangle r5 = new Rectangle(4, 4, 32, 22);

        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));
    }

    @Test
    public void testIs5RectanglesOverlappingByPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-5, -5, 80, 865);
        Rectangle r3 = new Rectangle(-2, -6, 21, 63);
        Rectangle r4 = new Rectangle(1, 1, 8, 9);
        Rectangle r5 = new Rectangle(4, 4, 32, 22);

        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));
    }

    @Test
    public void testIs5RectanglesOverlappingBy2RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(23, 23, 21, 63);
        Rectangle r4 = new Rectangle(322, 432, 8, 9);
        Rectangle r5 = new Rectangle(4048, 2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy2RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(-23, -23, 21, 63);
        Rectangle r4 = new Rectangle(322, 432, 8, 9);
        Rectangle r5 = new Rectangle(-4048, -2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy2RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(-1, 1, 1, 1);
        Rectangle r3 = new Rectangle(-23, -23, 21, 63);
        Rectangle r4 = new Rectangle(-322, -432, 8, 9);
        Rectangle r5 = new Rectangle(-4048, -2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy2RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(8, 8, 6, 6);
        Rectangle r3 = new Rectangle(93, 23, 21, 63);
        Rectangle r4 = new Rectangle(322, 432, 8, 9);
        Rectangle r5 = new Rectangle(4048, 2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy2RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(8, 8, 6, 6);
        Rectangle r3 = new Rectangle(-93, 23, 21, 63);
        Rectangle r4 = new Rectangle(322, -432, 8, 9);
        Rectangle r5 = new Rectangle(-4048, 2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy2RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(-1, -1, 6, 6);
        Rectangle r3 = new Rectangle(-93, -23, 21, 63);
        Rectangle r4 = new Rectangle(-322, -432, 8, 9);
        Rectangle r5 = new Rectangle(-4048, 2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy3RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(2, 2, 6, 6);
        Rectangle r3 = new Rectangle(3, 3, 1, 3);
        Rectangle r4 = new Rectangle(322, 432, 8, 9);
        Rectangle r5 = new Rectangle(4048, 2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy3RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-2, -2, 6, 6);
        Rectangle r3 = new Rectangle(3, 3, 1, 1);
        Rectangle r4 = new Rectangle(322, -432, 8, 9);
        Rectangle r5 = new Rectangle(-4048, 2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy3RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(-2, -2, 6, 6);
        Rectangle r3 = new Rectangle(-3, -3, 23, 123);
        Rectangle r4 = new Rectangle(-322, -432, 8, 9);
        Rectangle r5 = new Rectangle(-4048, -2014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy3RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(1, 1, 10, 10);
        Rectangle r2 = new Rectangle(0, 0, 2, 2);
        Rectangle r3 = new Rectangle(0, 1, 23, 123);
        Rectangle r4 = new Rectangle(456, 45532, 844, 239);
        Rectangle r5 = new Rectangle(34048, 6014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy3RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(0, 0, 2, 2);
        Rectangle r3 = new Rectangle(0, -1, 23, 123);
        Rectangle r4 = new Rectangle(456, -45532, 844, 239);
        Rectangle r5 = new Rectangle(34048, -6014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy3RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(-40, -30, 42, 32);
        Rectangle r3 = new Rectangle(-2, -1, 3423, 1323);
        Rectangle r4 = new Rectangle(-456, -45532, 844, 239);
        Rectangle r5 = new Rectangle(-34048, -6014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy4RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(34048, 6014, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy4RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-1, -1, 9, 9);
        Rectangle r3 = new Rectangle(2, -2, 8, 8);
        Rectangle r4 = new Rectangle(-3, 3, 7, 7);
        Rectangle r5 = new Rectangle(3048, 64, 32, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy4RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 5, 5);
        Rectangle r2 = new Rectangle(-1, -1, 9, 9);
        Rectangle r3 = new Rectangle(-2, -2, 8, 8);
        Rectangle r4 = new Rectangle(-3, -3, 7, 7);
        Rectangle r5 = new Rectangle(348, 624, 324, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy4RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(1, 1, 5, 5);
        Rectangle r2 = new Rectangle(3, 3, 9, 9);
        Rectangle r3 = new Rectangle(5, 5, 8, 8);
        Rectangle r4 = new Rectangle(6, 6, 342, 70);
        Rectangle r5 = new Rectangle(348, 624, 324, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy4RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(1, 1, 5, 5);
        Rectangle r2 = new Rectangle(-3, 3, 9, 9);
        Rectangle r3 = new Rectangle(5, 5, 8, 8);
        Rectangle r4 = new Rectangle(6, 6, 342, 70);
        Rectangle r5 = new Rectangle(348, -624, 324, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingBy4RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 5, 5);
        Rectangle r2 = new Rectangle(-3, -3, 93, 9);
        Rectangle r3 = new Rectangle(-5, -5, 899, 8);
        Rectangle r4 = new Rectangle(-6, -6, 342, 70);
        Rectangle r5 = new Rectangle(3438, -62234, 324, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingByNoOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 5, 5);
        Rectangle r2 = new Rectangle(6, 6, 4, 4);
        Rectangle r3 = new Rectangle(12, 12, 8, 8);
        Rectangle r4 = new Rectangle(34, 54, 342, 70);
        Rectangle r5 = new Rectangle(3438, 2234, 324, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -2, 5, 5);
        Rectangle r2 = new Rectangle(6, 6, 4, 4);
        Rectangle r3 = new Rectangle(-212, 12, 8, 8);
        Rectangle r4 = new Rectangle(342, -54, 342, 70);
        Rectangle r5 = new Rectangle(3438, -2234, 324, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs5RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -2, 1, 1);
        Rectangle r2 = new Rectangle(-6, -6, 4, 4);
        Rectangle r3 = new Rectangle(-212, -12, 8, 8);
        Rectangle r4 = new Rectangle(-342, -54, 342, 70);
        Rectangle r5 = new Rectangle(-3438, -2234, 324, 22);

        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r2, r3, r4, r5, r1));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r3, r4, r5, r1, r2));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r4, r5, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is5RectanglesOverlapping(r5, r1, r2, r3, r4));

    }

    @Test
    public void testIs6RectanglesOverlappingByFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);

        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-1, -1, 11, 11);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(-3, 3, 70, 70);
        Rectangle r5 = new Rectangle(4, 4, 9, 9);
        Rectangle r6 = new Rectangle(-1, 0, 6, 5);

        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingByFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-11, -11, 10, 10);
        Rectangle r2 = new Rectangle(-12, -12, 11, 11);
        Rectangle r3 = new Rectangle(-2, -2, 8, 8);
        Rectangle r4 = new Rectangle(-3, -3, 70, 70);
        Rectangle r5 = new Rectangle(-4, -4, 9, 9);
        Rectangle r6 = new Rectangle(-9, -6, 8, 5);

        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingByPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 90, 90);
        Rectangle r3 = new Rectangle(2, 2, 800, 800);
        Rectangle r4 = new Rectangle(3, 3, 7879, 9877);
        Rectangle r5 = new Rectangle(4, 4, 665467, 676766);
        Rectangle r6 = new Rectangle(5, 5, 556565656, 576565678);

        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingByPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, -2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 60, 67);
        Rectangle r6 = new Rectangle(5, -5, 5, 52);

        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingByPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-8, -8, 8, 8);
        Rectangle r3 = new Rectangle(-10, -8, 8, 8);
        Rectangle r4 = new Rectangle(-6, -3, 7, 97);
        Rectangle r5 = new Rectangle(-6, -4, 60, 67);
        Rectangle r6 = new Rectangle(-5, -5, 5, 52);

        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy2RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(8, 8, 1, 1);
        Rectangle r3 = new Rectangle(10, 10, 8, 8);
        Rectangle r4 = new Rectangle(22, 23, 7, 97);
        Rectangle r5 = new Rectangle(66, 404, 60, 67);
        Rectangle r6 = new Rectangle(454, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy2RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-8, -8, 2, 2);
        Rectangle r3 = new Rectangle(-8, -8, 1, 1);
        Rectangle r4 = new Rectangle(22, -23, 7, 97);
        Rectangle r5 = new Rectangle(-66, 404, 60, 67);
        Rectangle r6 = new Rectangle(-454, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy2RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(-8, -8, 2, 2);
        Rectangle r3 = new Rectangle(-8, -8, 1, 1);
        Rectangle r4 = new Rectangle(-22, -23, 7, 7);
        Rectangle r5 = new Rectangle(-66, -404, 30, 67);
        Rectangle r6 = new Rectangle(-454, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy2RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 2, 200);
        Rectangle r3 = new Rectangle(23, 28, 1, 1);
        Rectangle r4 = new Rectangle(222, 423, 7, 7);
        Rectangle r5 = new Rectangle(3466, 34404, 30, 67);
        Rectangle r6 = new Rectangle(454, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy2RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, -1, 10, 10);
        Rectangle r2 = new Rectangle(-1, -1, 2, 200);
        Rectangle r3 = new Rectangle(23, -28, 1, 1);
        Rectangle r4 = new Rectangle(222, -423, 7, 7);
        Rectangle r5 = new Rectangle(3466, -34404, 30, 67);
        Rectangle r6 = new Rectangle(454, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy2RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(-1, -1, 2, 200);
        Rectangle r3 = new Rectangle(-23, -28, 1, 1);
        Rectangle r4 = new Rectangle(-222, -423, 7, 7);
        Rectangle r5 = new Rectangle(-3466, -34404, 30, 67);
        Rectangle r6 = new Rectangle(-454, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy3RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(222, 423, 7, 7);
        Rectangle r5 = new Rectangle(3466, 34404, 30, 67);
        Rectangle r6 = new Rectangle(454, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy3RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, -1, 10, 10);
        Rectangle r2 = new Rectangle(-1, -1, 2, 200);
        Rectangle r3 = new Rectangle(-2, 0, 33, 9);
        Rectangle r4 = new Rectangle(222, -423, 7, 7);
        Rectangle r5 = new Rectangle(3466, -34404, 30, 67);
        Rectangle r6 = new Rectangle(454, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy3RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-222, -423, 7, 7);
        Rectangle r5 = new Rectangle(-3466, -34404, 30, 67);
        Rectangle r6 = new Rectangle(-454, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy3RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 1, 11);
        Rectangle r2 = new Rectangle(0, 1, 1, 45);
        Rectangle r3 = new Rectangle(0, 0, 8, 8);
        Rectangle r4 = new Rectangle(222, 423, 7, 7);
        Rectangle r5 = new Rectangle(3466, 34404, 30, 67);
        Rectangle r6 = new Rectangle(454, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy3RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 1, 11);
        Rectangle r2 = new Rectangle(-1, -1, 10, 45);
        Rectangle r3 = new Rectangle(0, 0, 8, 8);
        Rectangle r4 = new Rectangle(-222, 423, 7, 7);
        Rectangle r5 = new Rectangle(3466, -34404, 30, 67);
        Rectangle r6 = new Rectangle(454, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy3RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 11);
        Rectangle r2 = new Rectangle(-1, -1, 10, 45);
        Rectangle r3 = new Rectangle(-1, -1, 8, 8);
        Rectangle r4 = new Rectangle(-222, -423, 7, 7);
        Rectangle r5 = new Rectangle(-3466, -34404, 30, 67);
        Rectangle r6 = new Rectangle(-454, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy4RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 4, 2, 2);
        Rectangle r5 = new Rectangle(34, 404, 30, 67);
        Rectangle r6 = new Rectangle(45, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy4RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-4, -4, 10, 10);
        Rectangle r2 = new Rectangle(-1, 1, 1, 2);
        Rectangle r3 = new Rectangle(-2, -2, 8, 8);
        Rectangle r4 = new Rectangle(-3, -4, 6, 8);
        Rectangle r5 = new Rectangle(34, 404, 30, 67);
        Rectangle r6 = new Rectangle(45, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy4RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 6, 6);
        Rectangle r5 = new Rectangle(-34, -404, 30, 67);
        Rectangle r6 = new Rectangle(-45, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy4RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 11, 11);
        Rectangle r3 = new Rectangle(2, 2, 12, 12);
        Rectangle r4 = new Rectangle(3, 3, 13, 13);
        Rectangle r5 = new Rectangle(34, 404, 30, 67);
        Rectangle r6 = new Rectangle(45, 800, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy4RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-1, 1, 11, 11);
        Rectangle r3 = new Rectangle(2, -2, 12, 12);
        Rectangle r4 = new Rectangle(-3, 3, 13, 13);
        Rectangle r5 = new Rectangle(34, -404, 30, 67);
        Rectangle r6 = new Rectangle(-45, 800, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy4RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-12, -12, 8, 7);
        Rectangle r5 = new Rectangle(-34, -404, 30, 67);
        Rectangle r6 = new Rectangle(-45, -800, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy5RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 4, 2, 2);
        Rectangle r5 = new Rectangle(4, 4, 2, 1);
        Rectangle r6 = new Rectangle(45, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy5RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-1, -1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(-3, -4, 20, 20);
        Rectangle r5 = new Rectangle(4, 4, 2, 1);
        Rectangle r6 = new Rectangle(45, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy5RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 6, 6);
        Rectangle r5 = new Rectangle(-6, -6, 5, 5);
        Rectangle r6 = new Rectangle(-45, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy5RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 2, 9, 9);
        Rectangle r3 = new Rectangle(2, 3, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 6, 6);
        Rectangle r5 = new Rectangle(2, 1, 5, 5);
        Rectangle r6 = new Rectangle(45, 500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy5RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, -19, 100, 100);
        Rectangle r2 = new Rectangle(0, 1, 21, 32);
        Rectangle r3 = new Rectangle(-6, 0, 44, 800);
        Rectangle r4 = new Rectangle(-12, -3, 34, 444);
        Rectangle r5 = new Rectangle(0, 0, 8, 9);
        Rectangle r6 = new Rectangle(45, -500, 5, 52);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingBy5RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-100, -100, 101, 101);
        Rectangle r2 = new Rectangle(-110, -105, 100, 99);
        Rectangle r3 = new Rectangle(-90, -81, 342, 234);
        Rectangle r4 = new Rectangle(-84, -84, 222, 222);
        Rectangle r5 = new Rectangle(-74, -74, 111, 111);
        Rectangle r6 = new Rectangle(-1024, -1024, 2, 2);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingByNoOverlapping() {
        Rectangle r1 = new Rectangle(1, 1, 1, 1);
        Rectangle r2 = new Rectangle(3, 3, 3, 3);
        Rectangle r3 = new Rectangle(9, 9, 9, 9);
        Rectangle r4 = new Rectangle(23, 23, 23, 23);
        Rectangle r5 = new Rectangle(56, 56, 56, 56);
        Rectangle r6 = new Rectangle(456, 456, 456, 456);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, 1, 1, 1);
        Rectangle r2 = new Rectangle(-3, 3, 2, 2);
        Rectangle r3 = new Rectangle(-9, 9, 1, 1);
        Rectangle r4 = new Rectangle(-23, 23, 2, 23);
        Rectangle r5 = new Rectangle(-56, 56, 6, 56);
        Rectangle r6 = new Rectangle(-456, 456, 6, 456);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs6RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-3, -3, 2, 2);
        Rectangle r3 = new Rectangle(-9, -9, 1, 1);
        Rectangle r4 = new Rectangle(-23, -23, 2, 3);
        Rectangle r5 = new Rectangle(-56, -56, 6, 5);
        Rectangle r6 = new Rectangle(-456, -456, 6, 6);

        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r2, r3, r4, r5, r6, r1));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r3, r4, r5, r6, r1, r2));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r4, r5, r6, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r5, r6, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is6RectanglesOverlapping(r6, r1, r2, r3, r4, r5));
    }

    @Test
    public void testIs7RectanglesOverlappingByFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 12, 12);
        Rectangle r2 = new Rectangle(0, 0, 12, 11);
        Rectangle r3 = new Rectangle(1, 1, 8, 8);
        Rectangle r4 = new Rectangle(2, 2, 7, 7);
        Rectangle r5 = new Rectangle(3, 3, 6, 6);
        Rectangle r6 = new Rectangle(4, 4, 5, 5);
        Rectangle r7 = new Rectangle(5, 5, 4, 4);

        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 4, 6);
        Rectangle r2 = new Rectangle(0, 0, 5, 5);
        Rectangle r3 = new Rectangle(-2, -2, 7, 7);
        Rectangle r4 = new Rectangle(0, 0, 4, 4);
        Rectangle r5 = new Rectangle(0, 0, 3, 3);
        Rectangle r6 = new Rectangle(-3, -3, 23, 21);
        Rectangle r7 = new Rectangle(1, 1, 1, 1);

        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingByFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 3, 3);
        Rectangle r3 = new Rectangle(-4, -4, 7, 7);
        Rectangle r4 = new Rectangle(-7, -7, 10, 10);
        Rectangle r5 = new Rectangle(-8, -8, 14, 14);
        Rectangle r6 = new Rectangle(-10, -10, 200, 120);
        Rectangle r7 = new Rectangle(-54, -63, 254, 263);

        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingByPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 12, 12);
        Rectangle r3 = new Rectangle(2, 2, 12, 12);
        Rectangle r4 = new Rectangle(3, 3, 12, 12);
        Rectangle r5 = new Rectangle(4, 4, 12, 12);
        Rectangle r6 = new Rectangle(5, 5, 12, 12);
        Rectangle r7 = new Rectangle(6, 6, 12, 12);

        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingByPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 100, 100);
        Rectangle r2 = new Rectangle(2, 2, 123, 123);
        Rectangle r3 = new Rectangle(3, 3, 234, 234);
        Rectangle r4 = new Rectangle(-4, 5, 45, 54);
        Rectangle r5 = new Rectangle(0, 0, 50, 50);
        Rectangle r6 = new Rectangle(-1, 1, 99, 98);
        Rectangle r7 = new Rectangle(3, -2, 67, 69);

        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingByPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-4, -4, 4, 4);
        Rectangle r5 = new Rectangle(-5, -5, 5, 5);
        Rectangle r6 = new Rectangle(-6, -6, 6, 6);
        Rectangle r7 = new Rectangle(-7, -7, 7, 7);

        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy2RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 1, 1);
        Rectangle r2 = new Rectangle(0, 0, 2, 2);
        Rectangle r3 = new Rectangle(3, 3, 1, 1);
        Rectangle r4 = new Rectangle(5, 5, 1, 1);
        Rectangle r5 = new Rectangle(7, 7, 2, 2);
        Rectangle r6 = new Rectangle(33, 33, 12, 12);
        Rectangle r7 = new Rectangle(76, 76, 12, 21);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy2RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 2, 2);
        Rectangle r2 = new Rectangle(-2, -2, 4, 4);
        Rectangle r3 = new Rectangle(4, 4, 5, 6);
        Rectangle r4 = new Rectangle(-8, 12, 1, 4);
        Rectangle r5 = new Rectangle(34, -23, 1, 1);
        Rectangle r6 = new Rectangle(432, 234, 3, 4);
        Rectangle r7 = new Rectangle(-12, -12, 4, 3);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy2RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-3, -3, 1, 1);
        Rectangle r4 = new Rectangle(-4, -4, 1, 1);
        Rectangle r5 = new Rectangle(-5, -5, 1, 1);
        Rectangle r6 = new Rectangle(-6, -6, 1, 1);
        Rectangle r7 = new Rectangle(-7, -7, 1, 1);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy2RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(8, 8, 4, 4);
        Rectangle r3 = new Rectangle(14, 14, 2, 2);
        Rectangle r4 = new Rectangle(21, 21, 2, 2);
        Rectangle r5 = new Rectangle(31, 31, 4, 4);
        Rectangle r6 = new Rectangle(43, 43, 5, 5);
        Rectangle r7 = new Rectangle(56, 65, 2, 3);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy2RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, 1, 12, 12);
        Rectangle r2 = new Rectangle(-2, 1, 3, 4);
        Rectangle r3 = new Rectangle(34, 43, 12, 12);
        Rectangle r4 = new Rectangle(-43, -23, 1, 1);
        Rectangle r5 = new Rectangle(-123, 123, 4, 5);
        Rectangle r6 = new Rectangle(-456, 654, 5, 6);
        Rectangle r7 = new Rectangle(-678, 699, 1, 34);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy2RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 2, 2);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-5, -5, 1, 1);
        Rectangle r4 = new Rectangle(-12, -12, 2, 3);
        Rectangle r5 = new Rectangle(-32, -43, 1, 1);
        Rectangle r6 = new Rectangle(-345, -543, 6, 7);
        Rectangle r7 = new Rectangle(-2345, -5432, 123, 345);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy3RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(3, 3, 2, 2);
        Rectangle r4 = new Rectangle(12, 12, 2, 2);
        Rectangle r5 = new Rectangle(34, 34, 2, 3);
        Rectangle r6 = new Rectangle(41, 41, 21, 21);
        Rectangle r7 = new Rectangle(88, 78, 2, 57);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy3RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 2, 2);
        Rectangle r2 = new Rectangle(-1, -1, 1, 1);
        Rectangle r3 = new Rectangle(-4, -4, 4, 4);
        Rectangle r4 = new Rectangle(34, 43, 2, 2);
        Rectangle r5 = new Rectangle(56, 65, 1, 2);
        Rectangle r6 = new Rectangle(-56, -6, 6, 1);
        Rectangle r7 = new Rectangle(-98, 67, 7, 3);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy3RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-4, -4, 1, 1);
        Rectangle r5 = new Rectangle(-7, -7, 2, 2);
        Rectangle r6 = new Rectangle(-9, -9, 2, 1);
        Rectangle r7 = new Rectangle(-12, -23, 2, 6);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy3RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 1, 10);
        Rectangle r2 = new Rectangle(0, 0, 2, 12);
        Rectangle r3 = new Rectangle(0, 0, 1, 23);
        Rectangle r4 = new Rectangle(32, 32, 1, 1);
        Rectangle r5 = new Rectangle(34, 34, 2, 2);
        Rectangle r6 = new Rectangle(43, 43, 2, 1);
        Rectangle r7 = new Rectangle(56, 56, 21, 32);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy3RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 20, 20);
        Rectangle r2 = new Rectangle(-5, -5, 26, 26);
        Rectangle r3 = new Rectangle(0, 0, 2, 3);
        Rectangle r4 = new Rectangle(123, -123, 2, 34);
        Rectangle r5 = new Rectangle(345, 54, 1, 2);
        Rectangle r6 = new Rectangle(-1243, -342, 3, 9);
        Rectangle r7 = new Rectangle(45, -98, 4, 1);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy3RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 10, 10);
        Rectangle r2 = new Rectangle(-2, -2, 8, 8);
        Rectangle r3 = new Rectangle(-3, -3, 7, 7);
        Rectangle r4 = new Rectangle(-12, -12, 1, 2);
        Rectangle r5 = new Rectangle(-23, -23, 2, 1);
        Rectangle r6 = new Rectangle(-54, -45, 3, 4);
        Rectangle r7 = new Rectangle(-67, -76, 3, 1);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy4RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 12, 12);
        Rectangle r2 = new Rectangle(1, 1, 11, 11);
        Rectangle r3 = new Rectangle(2, 2, 9, 9);
        Rectangle r4 = new Rectangle(3, 3, 3, 3);
        Rectangle r5 = new Rectangle(22, 22, 1, 2);
        Rectangle r6 = new Rectangle(33, 33, 2, 1);
        Rectangle r7 = new Rectangle(44, 44, 1, 1);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy4RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 11, 11);
        Rectangle r2 = new Rectangle(-2, -2, 13, 13);
        Rectangle r3 = new Rectangle(-3, -3, 14, 14);
        Rectangle r4 = new Rectangle(-4, 1, 2, 3);
        Rectangle r5 = new Rectangle(-12, 43, 1, 2);
        Rectangle r6 = new Rectangle(300, 101, 2, 1);
        Rectangle r7 = new Rectangle(-300, 101, 1, 2);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy4RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 2, 2);
        Rectangle r2 = new Rectangle(-2, -2, 3, 3);
        Rectangle r3 = new Rectangle(-4, -4, 6, 6);
        Rectangle r4 = new Rectangle(-6, -6, 8, 8);
        Rectangle r5 = new Rectangle(-123, -123, 12, 12);
        Rectangle r6 = new Rectangle(-234, -234, 34, 34);
        Rectangle r7 = new Rectangle(-12, -12, 1, 1);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy4RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 2, 2);
        Rectangle r2 = new Rectangle(1, 1, 3, 3);
        Rectangle r3 = new Rectangle(1, 0, 12, 12);
        Rectangle r4 = new Rectangle(0, 1, 23, 43);
        Rectangle r5 = new Rectangle(54, 34, 1, 2);
        Rectangle r6 = new Rectangle(67, 56, 5, 6);
        Rectangle r7 = new Rectangle(121, 212, 22, 11);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy4RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 5, 6);
        Rectangle r2 = new Rectangle(0, 0, 10, 12);
        Rectangle r3 = new Rectangle(-1, 0, 23, 23);
        Rectangle r4 = new Rectangle(0, -1, 33, 43);
        Rectangle r5 = new Rectangle(-222, -222, 22, 22);
        Rectangle r6 = new Rectangle(333, -333, 22, 33);
        Rectangle r7 = new Rectangle(-1234, -2345, 12, 21);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy4RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-8, -8, 12, 12);
        Rectangle r3 = new Rectangle(-6, -5, 60, 50);
        Rectangle r4 = new Rectangle(-1, -1, 2, 222);
        Rectangle r5 = new Rectangle(-222, -222, 22, 22);
        Rectangle r6 = new Rectangle(-454, -545, 12, 21);
        Rectangle r7 = new Rectangle(-2048, -2049, 48, 49);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy5RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(2, 2, 7, 7);
        Rectangle r4 = new Rectangle(3, 3, 6, 6);
        Rectangle r5 = new Rectangle(4, 4, 5, 5);
        Rectangle r6 = new Rectangle(10, 10, 2, 2);
        Rectangle r7 = new Rectangle(13, 13, 2, 2);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy5RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 20, 20);
        Rectangle r2 = new Rectangle(-9, -9, 34, 34);
        Rectangle r3 = new Rectangle(1, -2, 10, 10);
        Rectangle r4 = new Rectangle(-7, -7, 10, 10);
        Rectangle r5 = new Rectangle(-1, -1, 2, 2);
        Rectangle r6 = new Rectangle(34, 43, 2, 1);
        Rectangle r7 = new Rectangle(-765, 456, 6, 8);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy5RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-4, -4, 4, 4);
        Rectangle r5 = new Rectangle(-5, -5, 5, 5);
        Rectangle r6 = new Rectangle(-6, -6, 1, 1);
        Rectangle r7 = new Rectangle(-8, -8, 1, 2);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy5RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(2, 2, 12, 12);
        Rectangle r3 = new Rectangle(3, 3, 32, 32);
        Rectangle r4 = new Rectangle(4, 4, 12, 12);
        Rectangle r5 = new Rectangle(5, 5, 200, 321);
        Rectangle r6 = new Rectangle(1234, 9876, 12, 43);
        Rectangle r7 = new Rectangle(98765, 43210, 1, 2);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy5RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, 0, 100, 100);
        Rectangle r2 = new Rectangle(-9, 0, 99, 101);
        Rectangle r3 = new Rectangle(-9, 1, 98, 102);
        Rectangle r4 = new Rectangle(-8, 2, 97, 203);
        Rectangle r5 = new Rectangle(-7, 0, 12, 100);
        Rectangle r6 = new Rectangle(345, -987, 12, 43);
        Rectangle r7 = new Rectangle(876, -468, 2, 3);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy5RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-12, -12, 10, 10);
        Rectangle r3 = new Rectangle(-14, -14, 8, 8);
        Rectangle r4 = new Rectangle(-16, -16, 8, 8);
        Rectangle r5 = new Rectangle(-18, -18, 10, 10);
        Rectangle r6 = new Rectangle(-20, -20, 2, 2);
        Rectangle r7 = new Rectangle(-22, -22, 1, 2);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy6RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(11, 11, 1,1);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy6RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-1, 1, 1, 1);
        Rectangle r3 = new Rectangle(-1, -1, 3, 4);
        Rectangle r4 = new Rectangle(-2, -2, 80, 54);
        Rectangle r5 = new Rectangle(0, 0, 10, 20);
        Rectangle r6 = new Rectangle(-10, -20, 40, 61);
        Rectangle r7 = new Rectangle(32, 54, 1, 1);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy6RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-4, -4, 4, 4);
        Rectangle r5 = new Rectangle(-5, -5, 5, 5);
        Rectangle r6 = new Rectangle(-6, -6, 6, 6);
        Rectangle r7 = new Rectangle(-10, -10, 2, 2);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy6RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 11, 11);
        Rectangle r3 = new Rectangle(2, 2, 12, 12);
        Rectangle r4 = new Rectangle(3, 3, 13, 13);
        Rectangle r5 = new Rectangle(4, 4, 14, 14);
        Rectangle r6 = new Rectangle(5, 5, 15, 15);
        Rectangle r7 = new Rectangle(20, 20, 22, 22);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy6RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 20, 20);
        Rectangle r2 = new Rectangle(0, 0, 20, 20);
        Rectangle r3 = new Rectangle(-9, -9, 20, 20);
        Rectangle r4 = new Rectangle(1, 1, 34, 43);
        Rectangle r5 = new Rectangle(2, -2, 32, 23);
        Rectangle r6 = new Rectangle(1, -1, 120, 220);
        Rectangle r7 = new Rectangle(-345, -1000, 22, 12);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingBy6RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 11, 11);
        Rectangle r2 = new Rectangle(-8, -8, 11, 15);
        Rectangle r3 = new Rectangle(-7, -7, 14, 9);
        Rectangle r4 = new Rectangle(-6, -6, 10, 10);
        Rectangle r5 = new Rectangle(-5, -5, 6, 6);
        Rectangle r6 = new Rectangle(-4, -4, 5, 5);
        Rectangle r7 = new Rectangle(-12, -12, 1, 1);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingByNoOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 1, 1);
        Rectangle r2 = new Rectangle(2, 2, 1, 1);
        Rectangle r3 = new Rectangle(4, 4, 1, 1);
        Rectangle r4 = new Rectangle(6, 6, 1, 1);
        Rectangle r5 = new Rectangle(8, 8, 1, 1);
        Rectangle r6 = new Rectangle(12, 12, 1, 1);
        Rectangle r7 = new Rectangle(14, 14, 2, 2);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-100, -100, 10, 10);
        Rectangle r2 = new Rectangle(100, 100, 10 ,10);
        Rectangle r3 = new Rectangle(-80, -80, 10, 10);
        Rectangle r4 = new Rectangle(80, 80, 5, 5);
        Rectangle r5 = new Rectangle(-70, 70, 2, 2);
        Rectangle r6 = new Rectangle(70, -70, 3, 3);
        Rectangle r7 = new Rectangle(1000, -1000, 100, 100);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs7RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-20, -20, 2, 2);
        Rectangle r3 = new Rectangle(-30, -30, 3, 3);
        Rectangle r4 = new Rectangle(-40, -40, 4, 4);
        Rectangle r5 = new Rectangle(-50, -50, 5, 5);
        Rectangle r6 = new Rectangle(-60, -60, 6, 6);
        Rectangle r7 = new Rectangle(-70, -70, 7, 7);

        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r1));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r3, r4, r5, r6, r7, r1, r2));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r4, r5, r6, r7, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r5, r6, r7, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r6, r7, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is7RectanglesOverlapping(r7, r1, r2, r3, r4, r5, r6));
    }

    @Test
    public void testIs8RectanglesOverlappingBy () {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(6, 6, 4, 4);
        Rectangle r8 = new Rectangle(7, 7, 3, 3);

        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 2, 2);
        Rectangle r2 = new Rectangle(-1, -1, 3, 3);
        Rectangle r3 = new Rectangle(-2, -2, 4, 4);
        Rectangle r4 = new Rectangle(1, 1, 1, 1);
        Rectangle r5 = new Rectangle(-3, -3, 6, 6);
        Rectangle r6 = new Rectangle(-4, -4, 8, 8);
        Rectangle r7 = new Rectangle(-6, -6, 9, 9);
        Rectangle r8 = new Rectangle(-7, -7, 21, 21);

        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingByFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-4, -4, 4, 4);
        Rectangle r5 = new Rectangle(-5, -5, 5, 5);
        Rectangle r6 = new Rectangle(-6, -6, 6, 6);
        Rectangle r7 = new Rectangle(-7, -7, 7, 7);
        Rectangle r8 = new Rectangle(-8, -8, 8, 8);

        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingByPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 100, 100);
        Rectangle r2 = new Rectangle(1, 1, 101, 101);
        Rectangle r3 = new Rectangle(2, 2, 202, 202);
        Rectangle r4 = new Rectangle(3, 3, 303, 303);
        Rectangle r5 = new Rectangle(4, 4, 404, 404);
        Rectangle r6 = new Rectangle(5, 5, 505, 505);
        Rectangle r7 = new Rectangle(6, 6, 606, 606);
        Rectangle r8 = new Rectangle(7, 7, 707, 707);

        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingByPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(0, 0, 100, 100);
        Rectangle r2 = new Rectangle(-1, -1, 99, 99);
        Rectangle r3 = new Rectangle(1, 1, 101, 101);
        Rectangle r4 = new Rectangle(3, 4, 50, 60);
        Rectangle r5 = new Rectangle(-3, -4, 55, 66);
        Rectangle r6 = new Rectangle(0, -1, 120, 130);
        Rectangle r7 = new Rectangle(-5, -6, 74, 86);
        Rectangle r8 = new Rectangle(5, 5, 12, 1400);

        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingByPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-11, -11, 10, 10);
        Rectangle r3 = new Rectangle(-12, -12, 10, 10);
        Rectangle r4 = new Rectangle(-13, -13, 10, 10);
        Rectangle r5 = new Rectangle(-14, -14, 10, 10);
        Rectangle r6 = new Rectangle(-15, -15, 10, 10);
        Rectangle r7 = new Rectangle(-16, -16, 10, 10);
        Rectangle r8 = new Rectangle(-17, -17, 10, 10);

        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy2RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 3, 3);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(4, 4, 1, 1);
        Rectangle r4 = new Rectangle(6, 6, 1, 1);
        Rectangle r5 = new Rectangle(7, 7, 1, 1);
        Rectangle r6 = new Rectangle(11, 11, 1, 1);
        Rectangle r7 = new Rectangle(33, 33, 1, 1);
        Rectangle r8 = new Rectangle(55, 55, 1, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy2RectsFullOverlappingAndMixedCoordinate(){
        Rectangle r1 = new Rectangle(-1, 1, 3, 3);
        Rectangle r2 = new Rectangle(0, 0, 1, 1);
        Rectangle r3 = new Rectangle(4, 4, 5, 5);
        Rectangle r4 = new Rectangle(9, 9, 2, 2);
        Rectangle r5 = new Rectangle(33, 33, 2, 2);
        Rectangle r6 = new Rectangle(-44, 44, 3, 3);
        Rectangle r7 = new Rectangle(66, -66, 5, 4);
        Rectangle r8 = new Rectangle(99, -99, 99, 99);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy2RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-6, -6, 6, 6);
        Rectangle r2 = new Rectangle(-4, -4, 2, 2);
        Rectangle r3 = new Rectangle(-10, -12, 1, 2);
        Rectangle r4 = new Rectangle(-16, -15, 1, 3);
        Rectangle r5 = new Rectangle(-20, -22, 3, 2);
        Rectangle r6 = new Rectangle(-30, -33, 2, 3);
        Rectangle r7 = new Rectangle(-45, -44, 4, 5);
        Rectangle r8 = new Rectangle(-66, -77, 3, 4);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy2RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(9, 9, 10, 10);
        Rectangle r3 = new Rectangle(12, 12, 1, 1);
        Rectangle r4 = new Rectangle(14, 14, 2, 2);
        Rectangle r5 = new Rectangle(15, 15, 5, 5);
        Rectangle r6 = new Rectangle(32, 32, 4, 4);
        Rectangle r7 = new Rectangle(39, 39, 2, 2);
        Rectangle r8 = new Rectangle(45, 45, 5, 7);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy2RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 4, 4);
        Rectangle r2 = new Rectangle(1, 1, 10, 10);
        Rectangle r3 = new Rectangle(12, 12, 3, 4);
        Rectangle r4 = new Rectangle(-34, -23, 3, 4);
        Rectangle r5 = new Rectangle(55, 65, 4, 5);
        Rectangle r6 = new Rectangle(-66, -66, 5, 6);
        Rectangle r7 = new Rectangle(100, 100, 1, 1);
        Rectangle r8 = new Rectangle(-100, -100, 2, 2);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy2RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 2, 2);
        Rectangle r2 = new Rectangle(-2, -2, 6, 10);
        Rectangle r3 = new Rectangle(-10, -10, 3, 3);
        Rectangle r4 = new Rectangle(-20, -20, 2, 2);
        Rectangle r5 = new Rectangle(-30, -30, 3, 3);
        Rectangle r6 = new Rectangle(-40, -40, 4, 4);
        Rectangle r7 = new Rectangle(-50, -50, 5, 5);
        Rectangle r8 = new Rectangle(-60, -60, 6, 6);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy3RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(11, 11, 1, 1);
        Rectangle r5 = new Rectangle(13, 13, 1, 1);
        Rectangle r6 = new Rectangle(15, 15, 1, 1);
        Rectangle r7 = new Rectangle(17, 17, 1, 1);
        Rectangle r8 = new Rectangle(19, 19, 1, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy3RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(0, 0, 6, 6);
        Rectangle r3 = new Rectangle(1, 1, 3, 3);
        Rectangle r4 = new Rectangle(-6, -6, 1, 1);
        Rectangle r5 = new Rectangle(7, 7, 2, 2);
        Rectangle r6 = new Rectangle(-9, 5, 3, 2);
        Rectangle r7 = new Rectangle(10, -7, 3, 2);
        Rectangle r8 = new Rectangle(100, -100, 1, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy3RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-6, -6, 7, 7);
        Rectangle r2 = new Rectangle(-4, -4, 5, 5);
        Rectangle r3 = new Rectangle(-3, -3, 1, 1);
        Rectangle r4 = new Rectangle(-10, -11, 2, 1);
        Rectangle r5 = new Rectangle(-101, -101, 3, 3);
        Rectangle r6 = new Rectangle(-200, -200, 3, 4);
        Rectangle r7 = new Rectangle(-300, -300, 3, 4);
        Rectangle r8 = new Rectangle(-500, -500, 100, 100);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy3RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(1, 1, 10, 10);
        Rectangle r2 = new Rectangle(5, 5, 15, 20);
        Rectangle r3 = new Rectangle(6, 6, 100, 23);
        Rectangle r4 = new Rectangle(1234, 1234, 2, 2);
        Rectangle r5 = new Rectangle(2345, 3453, 5, 5);
        Rectangle r6 = new Rectangle(6543, 3456, 3, 2);
        Rectangle r7 = new Rectangle(7654, 4567, 1, 1);
        Rectangle r8 = new Rectangle(10000, 10000, 1000, 1000);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy3RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 6, 6);
        Rectangle r2 = new Rectangle(-4, -4, 4, 4);
        Rectangle r3 = new Rectangle(-4, -3, 4, 4);
        Rectangle r4 = new Rectangle(1, 1, 1, 1);
        Rectangle r5 = new Rectangle(2, 2, 1, 1);
        Rectangle r6 = new Rectangle(-14, -24, 3, 4);
        Rectangle r7 = new Rectangle(-44, -44, 4, 4);
        Rectangle r8 = new Rectangle(55, 66, 5, 6);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy3RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 15, 15);
        Rectangle r2 = new Rectangle(-12, -12, 8, 8);
        Rectangle r3 = new Rectangle(-14, -14, 5, 5);
        Rectangle r4 = new Rectangle(-100, -100, 10, 10);
        Rectangle r5 = new Rectangle(-101, -101, 1, 1);
        Rectangle r6 = new Rectangle(-200, -200, 2, 2);
        Rectangle r7 = new Rectangle(-300, -300, 3, 3);
        Rectangle r8 = new Rectangle(-400, -400, 4, 4);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy4RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 1, 1);
        Rectangle r2 = new Rectangle(0, 0, 2, 2);
        Rectangle r3 = new Rectangle(0, 0, 3, 3);
        Rectangle r4 = new Rectangle(0, 0, 4, 4);
        Rectangle r5 = new Rectangle(5, 5, 1, 1);
        Rectangle r6 = new Rectangle(7, 7, 4, 4);
        Rectangle r7 = new Rectangle(12, 12, 2, 2);
        Rectangle r8 = new Rectangle(16, 16, 4, 4);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy4RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 6, 6);
        Rectangle r2 = new Rectangle(0, 0, 5, 5);
        Rectangle r3 = new Rectangle(1, 1, 4, 4);
        Rectangle r4 = new Rectangle(2, 2, 3, 3);
        Rectangle r5 = new Rectangle(-12, -12, 4, 5);
        Rectangle r6 = new Rectangle(12, 13, 4, 5);
        Rectangle r7 = new Rectangle(22, -22, 2, 2);
        Rectangle r8 = new Rectangle(-65, -44, 4, 9);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy4RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-4, -4, 4, 4);
        Rectangle r5 = new Rectangle(-6, -6, 1, 1);
        Rectangle r6 = new Rectangle(-8, -8, 1, 1);
        Rectangle r7 = new Rectangle(-12, -12, 2, 3);
        Rectangle r8 = new Rectangle(-20, -22, 3, 2);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy4RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10,10);
        Rectangle r2 = new Rectangle(2, 2, 10, 10);
        Rectangle r3 = new Rectangle(4, 4, 44, 55);
        Rectangle r4 = new Rectangle(5, 5, 1, 100);
        Rectangle r5 = new Rectangle(49, 106, 22, 11);
        Rectangle r6 = new Rectangle(80, 133, 1, 1);
        Rectangle r7 = new Rectangle(98, 144, 3, 4);
        Rectangle r8 = new Rectangle(102, 150, 102, 150);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy4RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 11, 11);
        Rectangle r2 = new Rectangle(0, 0, 15, 16);
        Rectangle r3 = new Rectangle(1, 2, 20, 22);
        Rectangle r4 = new Rectangle(1, -2, 12, 32);
        Rectangle r5 = new Rectangle(-110, -220, 20, 32);
        Rectangle r6 = new Rectangle(-110, 112, 1, 2);
        Rectangle r7 = new Rectangle(112, -221, 5, 1);
        Rectangle r8 = new Rectangle(-500, 500, 50, 50);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy4RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-8, -8, 12, 12);
        Rectangle r3 = new Rectangle(-12, -12, 20, 20);
        Rectangle r4 = new Rectangle(-2, -2, 2, 2);
        Rectangle r5 = new Rectangle(-20, -20, 3, 3);
        Rectangle r6 = new Rectangle(-30, -30, 3, 3);
        Rectangle r7 = new Rectangle(-45, -45, 5, 5);
        Rectangle r8 = new Rectangle(-88, -98, 1, 2);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy5RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(10, 10, 1, 1);
        Rectangle r7 = new Rectangle(22, 22, 3, 3);
        Rectangle r8 = new Rectangle(30, 30, 4, 3);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy5RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 12, 12);
        Rectangle r2 = new Rectangle(-1, -1, 10, 10);
        Rectangle r3 = new Rectangle(0, 0, 9, 9);
        Rectangle r4 = new Rectangle(1, 1, 8, 8);
        Rectangle r5 = new Rectangle(2, 2, 7, 7);
        Rectangle r6 = new Rectangle(-5, -4, 2, 1);
        Rectangle r7 = new Rectangle(12, 21, 3, 4);
        Rectangle r8 = new Rectangle(33, -33, 3, 3);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy5RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-11, -11, 1, 1);
        Rectangle r7 = new Rectangle(-45, -45, 5, 5);
        Rectangle r8 = new Rectangle(-98, -98, 9, 8);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy5RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 10, 10);
        Rectangle r3 = new Rectangle(2, 2, 10, 10);
        Rectangle r4 = new Rectangle(3, 3, 10, 10);
        Rectangle r5 = new Rectangle(4, 4, 10, 10);
        Rectangle r6 = new Rectangle(15, 15, 1, 1);
        Rectangle r7 = new Rectangle(20,20, 108, 103);
        Rectangle r8 = new Rectangle(322, 233, 4, 5);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy5RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 6, 6);
        Rectangle r2 = new Rectangle(-1, -1, 6, 6);
        Rectangle r3 = new Rectangle(0, 0, 6, 6);
        Rectangle r4 = new Rectangle(1, 1, 6, 6);
        Rectangle r5 = new Rectangle(2, 2, 6, 6);
        Rectangle r6 = new Rectangle(-9, 9, 2, 2);
        Rectangle r7 = new Rectangle(11, 11, 1, 1);
        Rectangle r8 = new Rectangle(14, -14, 1, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy5RectsPartOverlappingAndNegativeCoordinate(){
        Rectangle r1 = new Rectangle(-10, -10, 8, 8);
        Rectangle r2 = new Rectangle(-12, -12, 9, 9);
        Rectangle r3 = new Rectangle(-13, -13, 9, 9);
        Rectangle r4 = new Rectangle(-14, -13, 9, 9);
        Rectangle r5 = new Rectangle(-14, -15, 7, 6);
        Rectangle r6 = new Rectangle(-20, -20, 2, 2);
        Rectangle r7 = new Rectangle(-23, -23, 3, 3);
        Rectangle r8 = new Rectangle(-32, -32, 1, 2);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy6RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(12, 12, 2, 3);
        Rectangle r8 = new Rectangle(15, 16, 12, 21);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy6RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 13, 13);
        Rectangle r2 = new Rectangle(-2, -1, 15, 11);
        Rectangle r3 = new Rectangle(-1, -1, 11, 9);
        Rectangle r4 = new Rectangle(0, 0, 9, 7);
        Rectangle r5 = new Rectangle(2, 1, 5, 4);
        Rectangle r6 = new Rectangle(3, 3, 4, 6);
        Rectangle r7 = new Rectangle(40, 44, 8, 4);
        Rectangle r8 = new Rectangle(-120, -10, 20, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy6RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-5, -5, 5, 5);
        Rectangle r7 = new Rectangle(-14, -14, 2, 2);
        Rectangle r8 = new Rectangle(-21, -21, 3, 2);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy6RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 10, 10);
        Rectangle r3 = new Rectangle(2, 2, 10, 10);
        Rectangle r4 = new Rectangle(3, 3, 10, 10);
        Rectangle r5 = new Rectangle(4, 4, 10, 10);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(11, 11, 2, 2);
        Rectangle r8 = new Rectangle(15, 16, 2, 3);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy6RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 8, 8);
        Rectangle r2 = new Rectangle(-2, -2, 8, 8);
        Rectangle r3 = new Rectangle(0,-1, 7, 8);
        Rectangle r4 = new Rectangle(0, 0, 8, 8);
        Rectangle r5 = new Rectangle(1, 0, 10, 13);
        Rectangle r6 = new Rectangle(0, 1, 10, 13);
        Rectangle r7 = new Rectangle(-23, -22, 3, 2);
        Rectangle r8 = new Rectangle(23, 22, 2, 3);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy6RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-11, -11, 10, 10);
        Rectangle r3 = new Rectangle(-12, -12, 10, 10);
        Rectangle r4 = new Rectangle(-13, -13, 9, 9);
        Rectangle r5 = new Rectangle(-14, -14, 10, 10);
        Rectangle r6 = new Rectangle(-15, -15, 11, 16);
        Rectangle r7 = new Rectangle(-16, -16, 1, 1);
        Rectangle r8 = new Rectangle(-20, -20, 2, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy7RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(6, 6, 4, 4);
        Rectangle r8 = new Rectangle(12, 12, 2, 2);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy7RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 15, 15);
        Rectangle r2 = new Rectangle(-4, -4, 15, 15);
        Rectangle r3 = new Rectangle(-1, -1, 19, 19);
        Rectangle r4 = new Rectangle(0, 0,21, 21);
        Rectangle r5 = new Rectangle(1, 1, 3, 4);
        Rectangle r6 = new Rectangle(2, 2, 1, 18);
        Rectangle r7 = new Rectangle(-6, 0, 26, 23);
        Rectangle r8 = new Rectangle(-322, 322, 2, 4);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy7RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-20, -20, 20, 20);
        Rectangle r2 = new Rectangle(-19, -19, 18, 18);
        Rectangle r3 = new Rectangle(-18, -18, 16, 16);
        Rectangle r4 = new Rectangle(-17, -17, 14, 14);
        Rectangle r5 = new Rectangle(-16, -16, 12, 12);
        Rectangle r6 = new Rectangle(-14, -14, 9, 9);
        Rectangle r7 = new Rectangle(-9, -9, 1, 1);
        Rectangle r8 = new Rectangle(-99, -99, 9, 8);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy7RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 10, 10);
        Rectangle r4 = new Rectangle(3, 3, 11, 11);
        Rectangle r5 = new Rectangle(4, 4, 12, 12);
        Rectangle r6 = new Rectangle(5, 5, 13, 13);
        Rectangle r7 = new Rectangle(6, 6, 15, 15);
        Rectangle r8 = new Rectangle(25, 26, 12, 13);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy7RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-4, -4, 14, 14);
        Rectangle r2 = new Rectangle(-3, -3, 14, 14);
        Rectangle r3 = new Rectangle(-2, -1, 14, 13);
        Rectangle r4 = new Rectangle(0,0, 14, 14);
        Rectangle r5 = new Rectangle(1, 1, 15, 15);
        Rectangle r6 = new Rectangle(2, 2, 15, 15);
        Rectangle r7 = new Rectangle(3, 3, 120, 110 );
        Rectangle r8 = new Rectangle(220, 222, 10, 8);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingBy7RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-9, -9, 9, 9);
        Rectangle r2 = new Rectangle(-10, -10, 9, 9);
        Rectangle r3 = new Rectangle(-11, -11, 9, 9);
        Rectangle r4 = new Rectangle(-12, -12, 9, 9);
        Rectangle r5 = new Rectangle(-13, -13, 9, 9);
        Rectangle r6 = new Rectangle(-14, -14, 9, 9);
        Rectangle r7 = new Rectangle(-15, -15, 9, 9);
        Rectangle r8 = new Rectangle(-20, -20, 1, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingByNoOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 1, 1);
        Rectangle r2 = new Rectangle(2, 2, 1, 2);
        Rectangle r3 = new Rectangle(5, 5, 5, 5);
        Rectangle r4 = new Rectangle(11, 11, 2, 2);
        Rectangle r5 = new Rectangle(14, 14, 6, 6);
        Rectangle r6 = new Rectangle(22, 22, 1, 1);
        Rectangle r7 = new Rectangle(24, 24, 6, 6);
        Rectangle r8 = new Rectangle(41, 44, 6, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 2, 2);
        Rectangle r2 = new Rectangle(-10, 10, 2, 2);
        Rectangle r3 = new Rectangle(10, -10, 3, 3);
        Rectangle r4 = new Rectangle(10, 10, 4, 4);
        Rectangle r5 = new Rectangle(-1, -1, 1, 1);
        Rectangle r6 = new Rectangle(0, 0, 1, 1);
        Rectangle r7 = new Rectangle(-15, -16, 1, 2);
        Rectangle r8 = new Rectangle(20, 22, 4, 2);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs8RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-3, -3, 2, 2);
        Rectangle r3 = new Rectangle(-5, -5, 2, 2);
        Rectangle r4 = new Rectangle(-7, -7, 2, 2);
        Rectangle r5 = new Rectangle(-9, -9, 1, 2);
        Rectangle r6 = new Rectangle(-12, -12, 1, 2);
        Rectangle r7 = new Rectangle(-15, -16, 2, 4);
        Rectangle r8 = new Rectangle(-20, -20, 1, 1);

        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r1));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r1, r2));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r4, r5, r6, r7, r8, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r5, r6, r7, r8, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r6, r7, r8, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r7, r8, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is8RectanglesOverlapping(r8, r1, r2, r3, r4, r5, r6, r7));
    }

    @Test
    public void testIs9RectanglesOverlappingByFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(6, 6, 4, 4);
        Rectangle r8 = new Rectangle(7, 7, 3, 3);
        Rectangle r9 = new Rectangle(8, 8, 2, 2);

        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-4, -4, 9, 9);
        Rectangle r3 = new Rectangle(-3, -3, 8, 8);
        Rectangle r4 = new Rectangle(-2, -2, 7, 7);
        Rectangle r5 = new Rectangle(-1, -1, 6, 6);
        Rectangle r6 = new Rectangle(0, 0, 5, 5);
        Rectangle r7 = new Rectangle(1, 1, 4, 4);
        Rectangle r8 = new Rectangle(2, 2, 3, 3);
        Rectangle r9 = new Rectangle(3, 3, 2, 2);

        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingByFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-5, -5, 5, 5);
        Rectangle r7 = new Rectangle(-4, -4, 4, 4);
        Rectangle r8 = new Rectangle(-3, -3, 3, 3);
        Rectangle r9 = new Rectangle(-2, -2, 2, 2);

        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingByPartOverlapping(){
        Rectangle r1 = new Rectangle(0,0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 10, 10);
        Rectangle r3 = new Rectangle(2, 2, 11, 11);
        Rectangle r4 = new Rectangle(3, 3, 12, 12);
        Rectangle r5 = new Rectangle(4, 4, 13, 13);
        Rectangle r6 = new Rectangle(5, 5, 14, 14);
        Rectangle r7 = new Rectangle(6, 6, 15, 15);
        Rectangle r8 = new Rectangle(7, 7, 16, 16);
        Rectangle r9 = new Rectangle(8, 8, 17, 17);

        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingByPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-4, -4, 9, 9);
        Rectangle r2 = new Rectangle(-3, -3, 9, 9);
        Rectangle r3 = new Rectangle(-2, -2, 9, 9);
        Rectangle r4 = new Rectangle(-1, 0, 9, 9);
        Rectangle r5 = new Rectangle(0, 0, 9, 9);
        Rectangle r6 = new Rectangle(1, 1, 10, 10);
        Rectangle r7 = new Rectangle(2, 2, 12, 12);
        Rectangle r8 = new Rectangle(3, 2, 12, 13);
        Rectangle r9 = new Rectangle(1, -1, 22, 21);

        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingByPartOverlappingAndNegativeCoordinate(){
        Rectangle r1 = new Rectangle(-10, -10, 11, 11);
        Rectangle r2 = new Rectangle(-9, -9, 12, 12);
        Rectangle r3 = new Rectangle(-8, -8, 13, 13);
        Rectangle r4 = new Rectangle(-7, -7, 14, 14);
        Rectangle r5 = new Rectangle(-6, -6, 15, 15);
        Rectangle r6 = new Rectangle(-5, -5, 16, 16);
        Rectangle r7 = new Rectangle(-4, -4, 17, 17);
        Rectangle r8 = new Rectangle(-3, -3, 18, 18);
        Rectangle r9 = new Rectangle(-2, -2, 20, 20);

        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy2RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 2, 2);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(3, 3, 1, 1);
        Rectangle r4 = new Rectangle(5, 5, 1, 1);
        Rectangle r5 = new Rectangle(7, 7, 1, 1);
        Rectangle r6 = new Rectangle(9, 9, 1, 1);
        Rectangle r7 = new Rectangle(21, 21, 2, 2);
        Rectangle r8 = new Rectangle(24, 24, 1, 1);
        Rectangle r9 = new Rectangle(31, 31, 2, 4);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy2RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 3, 3);
        Rectangle r2 = new Rectangle(0, 0, 1, 1);
        Rectangle r3 = new Rectangle(-5, 5, 1, 1);
        Rectangle r4 = new Rectangle(5, -5, 1, 1);
        Rectangle r5 = new Rectangle(5, 5, 1, 1);
        Rectangle r6 = new Rectangle(-5, -5, 1, 1);
        Rectangle r7 = new Rectangle(10, 10, 2, 2);
        Rectangle r8 = new Rectangle(-10, -10, 12, 12);
        Rectangle r9 = new Rectangle(-12, -432, 2, 30);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy2RectsFullOverlappingAndNegativeCoordinate(){
        Rectangle r1 = new Rectangle(-5, -5, 5, 5);
        Rectangle r2 = new Rectangle(-4, -4, 3, 3);
        Rectangle r3 = new Rectangle(-7, -7, 1, 1);
        Rectangle r4 = new Rectangle(-10, -10, 2, 2);
        Rectangle r5 = new Rectangle(-13, -12, 3, 2);
        Rectangle r6 = new Rectangle(-15, -16, 1, 2);
        Rectangle r7 = new Rectangle(-20, -20, 1, 1);
        Rectangle r8 = new Rectangle(-33, -33, 3, 3);
        Rectangle r9 = new Rectangle(-40, -40, 1, 2);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy2RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 5, 5);
        Rectangle r2 = new Rectangle(4, 4, 2, 2);
        Rectangle r3 = new Rectangle(7, 7, 3, 3);
        Rectangle r4 = new Rectangle(14, 14, 4, 4);
        Rectangle r5 = new Rectangle(19, 19, 9, 9);
        Rectangle r6 = new Rectangle(30, 30, 3, 3);
        Rectangle r7 = new Rectangle(35, 35, 5, 5);
        Rectangle r8 = new Rectangle(41, 42, 4, 3);
        Rectangle r9 = new Rectangle(46, 46, 4, 5);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy2RectsPartOverlappingAndMixedCoordinate(){
        Rectangle r1 = new Rectangle(-3, -3, 6, 6);
        Rectangle r2 = new Rectangle(-1, 0, 8, 4);
        Rectangle r3 = new Rectangle(-5, 5, 1, 10);
        Rectangle r4 = new Rectangle(10, 10, 10, 10);
        Rectangle r5 = new Rectangle(-30, -30, 3, 3);
        Rectangle r6 = new Rectangle(-22, -22, 2, 2);
        Rectangle r7 = new Rectangle(22, -22, 2, 1);
        Rectangle r8 = new Rectangle(-100, -100, 1, 1);
        Rectangle r9 = new Rectangle(100, 100, 2, 3);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy2RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 5, 5);
        Rectangle r2 = new Rectangle(-1, -1, 2, 2);
        Rectangle r3 = new Rectangle(-8, -8, 2, 2);
        Rectangle r4 = new Rectangle(-11, -11, 1, 1);
        Rectangle r5 = new Rectangle(-15, -15, 2, 2);
        Rectangle r6 = new Rectangle(-20, -20, 3, 3);
        Rectangle r7 = new Rectangle(-30, -30, 4, 5);
        Rectangle r8 = new Rectangle(-35, -35, 2, 1);
        Rectangle r9 = new Rectangle(-40, -40, 5, 4);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));

    }

    @Test
    public void testIs9RectanglesOverlappingBy3RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 3, 3);
        Rectangle r2 = new Rectangle(1, 1, 2, 2);
        Rectangle r3 = new Rectangle(2, 2, 1, 1);
        Rectangle r4 = new Rectangle(4, 4, 1, 1);
        Rectangle r5 = new Rectangle(6, 6, 2, 2);
        Rectangle r6 = new Rectangle(9, 9, 1, 1);
        Rectangle r7 = new Rectangle(11, 11, 1, 1);
        Rectangle r8 = new Rectangle(15, 15, 5, 5);
        Rectangle r9 = new Rectangle(43, 34, 3, 2);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy3RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 6, 6);
        Rectangle r2 = new Rectangle(0, 0, 3, 3);
        Rectangle r3 = new Rectangle(1, 1, 1, 1);
        Rectangle r4 = new Rectangle(-6, 5, 1, 2);
        Rectangle r5 = new Rectangle(-5, 6, 1, 1);
        Rectangle r6 = new Rectangle(10, 10, 2, 2);
        Rectangle r7 = new Rectangle(-23, -23, 3, 4);
        Rectangle r8 = new Rectangle(43, 43, 43, 43);
        Rectangle r9 = new Rectangle(-55, -66, 5, 6);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy3RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 3, 3);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-1, -1, 1, 1);
        Rectangle r4 = new Rectangle(-5, -5, 1, 1);
        Rectangle r5 = new Rectangle(-8, -8, 2, 2);
        Rectangle r6 = new Rectangle(-11, -11, 2, 2);
        Rectangle r7 = new Rectangle(-15, -15, 1, 1);
        Rectangle r8 = new Rectangle(-20, -20, 1, 1);
        Rectangle r9 = new Rectangle(-55, -60, 4, 5);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy3RectsPartOverlapping(){
        Rectangle r1 = new Rectangle(0, 0, 5, 5);
        Rectangle r2 = new Rectangle(1, 1, 6, 6);
        Rectangle r3 = new Rectangle(2, 2, 7, 7);
        Rectangle r4 = new Rectangle(10, 10,1, 1);
        Rectangle r5 = new Rectangle(12, 12, 8, 8);
        Rectangle r6 = new Rectangle(22, 22, 3, 3);
        Rectangle r7 = new Rectangle(26, 26, 4, 4);
        Rectangle r8 = new Rectangle(31, 31, 2, 2);
        Rectangle r9 = new Rectangle(34, 34, 4, 4);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy3RectsPartOverlappingAndMixedCoordinate(){
        Rectangle r1 = new Rectangle(-1, -1, 3, 3);
        Rectangle r2 = new Rectangle(0, 0, 3, 3);
        Rectangle r3 = new Rectangle(1, 1, 5, 5);
        Rectangle r4 = new Rectangle(-4, -4, 2, 2);
        Rectangle r5 = new Rectangle(8, 8, 2, 2);
        Rectangle r6 = new Rectangle(-10, 10, 1, 1);
        Rectangle r7 = new Rectangle(10, -10, 1, 1);
        Rectangle r8 = new Rectangle(10, 10, 1, 1);
        Rectangle r9 = new Rectangle(10, 10, 1, 1);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy3RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 2, 2);
        Rectangle r2 = new Rectangle(-2, -2, 2, 2);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-9, -9, 1, 1);
        Rectangle r5 = new Rectangle(-10, -10, 1, 1);
        Rectangle r6 = new Rectangle(-13, -13, 2, 2);
        Rectangle r7 = new Rectangle(-20, 29, 1, 11);
        Rectangle r8 = new Rectangle(-40, -40, 4, 4);
        Rectangle r9 = new Rectangle(-50, -50, 5, 5);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy4RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 5, 5);
        Rectangle r2 = new Rectangle(1, 1, 4, 4);
        Rectangle r3 = new Rectangle(2, 2, 3, 3);
        Rectangle r4 = new Rectangle(3, 3, 1, 1);
        Rectangle r5 = new Rectangle(6, 6, 4, 4);
        Rectangle r6 = new Rectangle(12, 12, 2, 2);
        Rectangle r7 = new Rectangle(15, 15, 5, 5);
        Rectangle r8 = new Rectangle(22, 22, 2, 2);
        Rectangle r9 = new Rectangle(25, 25, 2, 1);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy4RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 4, 4);
        Rectangle r2 = new Rectangle(-1, -1, 4, 4);
        Rectangle r3 = new Rectangle(0, 0, 4, 4);
        Rectangle r4 = new Rectangle(1, 1, 5, 5);
        Rectangle r5 = new Rectangle(-9, -9, 2, 2);
        Rectangle r6 = new Rectangle(10, -10, 1, 1);
        Rectangle r7 = new Rectangle(-10, 10, 1, 1);
        Rectangle r8 = new Rectangle(-10, -10, 1, 1);
        Rectangle r9 = new Rectangle(-11, -11, 1, 1);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy4RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 5, 5);
        Rectangle r2 = new Rectangle(-4, -4, 4, 4);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-2, -2, 2, 2);
        Rectangle r5 = new Rectangle(-7, -7, 1, 1);
        Rectangle r6 = new Rectangle(-9, -9, 2, 2);
        Rectangle r7 = new Rectangle(-11, -11, 1, 1);
        Rectangle r8 = new Rectangle(-40, -40, 2, 2);
        Rectangle r9 = new Rectangle(-100, -100, 10, 10);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy4RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 5, 5);
        Rectangle r2 = new Rectangle(1, 1, 5, 5);
        Rectangle r3 = new Rectangle(2, 2, 5, 5);
        Rectangle r4 = new Rectangle(3, 3, 5, 5);
        Rectangle r5 = new Rectangle(9, 9, 1, 1);
        Rectangle r6 = new Rectangle(11, 11, 4, 4);
        Rectangle r7 = new Rectangle(21, 21, 4, 4);
        Rectangle r8 = new Rectangle(30, 30, 3, 3);
        Rectangle r9 = new Rectangle(35, 35, 2, 3);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy4RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 8, 8);
        Rectangle r2 = new Rectangle(-2, -2, 8, 8);
        Rectangle r3 = new Rectangle(0, 0, 10, 10);
        Rectangle r4 = new Rectangle(3, 3, 11, 11);
        Rectangle r5 = new Rectangle(-10, 110, 1, 20);
        Rectangle r6 = new Rectangle(-110, 10, 2, 4);
        Rectangle r7 = new Rectangle(-200, -200, 20, 20);
        Rectangle r8 = new Rectangle(500, 500, 50, 50);
        Rectangle r9 = new Rectangle(-900, -900, 20, 200);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy4RectsPartOverlappingAndNegativeCoordinate(){
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-11, -11, 10, 10);
        Rectangle r3 = new Rectangle(-12, -12, 10, 10);
        Rectangle r4 = new Rectangle(-14, -14, 10, 10);
        Rectangle r5 = new Rectangle(-20, -20, 1, 1);
        Rectangle r6 = new Rectangle(-30, -30, 3, 3);
        Rectangle r7 = new Rectangle(-40, -40, 4, 4);
        Rectangle r8 = new Rectangle(-50, -50, 5, 5);
        Rectangle r9 = new Rectangle(-60, -60, 6, 6);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy5RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(11, 11, 9, 9);
        Rectangle r7 = new Rectangle(21, 21, 9, 9);
        Rectangle r8 = new Rectangle(40, 44, 8, 4);
        Rectangle r9 = new Rectangle(50, 50, 50, 50);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy5RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 6, 6);
        Rectangle r2 = new Rectangle(-1, -1, 5, 5);
        Rectangle r3 = new Rectangle(0, 0, 4, 4);
        Rectangle r4 = new Rectangle(1, 1, 3, 3);
        Rectangle r5 = new Rectangle(2, 2, 2, 2);
        Rectangle r6 = new Rectangle(-19, -10, 9, 1);
        Rectangle r7 = new Rectangle(22, 23, 3, 2);
        Rectangle r8 = new Rectangle(-22, 24, 2, 1);
        Rectangle r9 = new Rectangle(50, -120, 1, 1);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy5RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-11, -11, 1, 1);
        Rectangle r7 = new Rectangle(-120, -120, 20, 20);
        Rectangle r8 = new Rectangle(-80, -80, 5, 5);
        Rectangle r9 = new Rectangle(-63, -5, 3, 2);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy5RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 8, 8);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 8, 8);
        Rectangle r5 = new Rectangle(4, 4, 10, 10);
        Rectangle r6 = new Rectangle(15, 15, 1, 1);
        Rectangle r7 = new Rectangle(20, 20, 2, 2);
        Rectangle r8 = new Rectangle(40, 40, 4, 4);
        Rectangle r9 = new Rectangle(55, 55, 5, 5);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy5RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-3, 3, 7, 10);
        Rectangle r3 = new Rectangle(0, 0, 15, 15);
        Rectangle r4 = new Rectangle(2, 2, 22, 22);
        Rectangle r5 = new Rectangle(3, 3, 32, 32);
        Rectangle r6 = new Rectangle(40, -40, 4, 4);
        Rectangle r7 = new Rectangle(-50, 50, 2, 3);
        Rectangle r8 = new Rectangle(-100, -100, 10, 10);
        Rectangle r9 = new Rectangle(2000, 2222, 222, 22);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy5RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-11, -11, 10, 10);
        Rectangle r3 = new Rectangle(-12, -12, 10, 10);
        Rectangle r4 = new Rectangle(-13, -13, 10, 10);
        Rectangle r5 = new Rectangle(-14, -14, 10, 10);
        Rectangle r6 = new Rectangle(-15, -15, 1, 1);
        Rectangle r7 = new Rectangle(-20, -20, 2, 2);
        Rectangle r8 = new Rectangle(-30, -30, 3, 3);
        Rectangle r9 = new Rectangle(-59, -59, 9, 9);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy6RectsFullOverlapping(){
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7,7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(10, 10, 1, 1);
        Rectangle r8 = new Rectangle(13, 13, 3, 3);
        Rectangle r9 = new Rectangle(20, 20, 23, 32);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));

    }

    @Test
    public void testIs9RectanglesOverlappingBy6RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-4, -4, 9, 9);
        Rectangle r3 = new Rectangle(-1, -1, 5, 5);
        Rectangle r4 = new Rectangle(0, 0, 4, 4);
        Rectangle r5 = new Rectangle(1, 1, 3, 3);
        Rectangle r6 = new Rectangle(2, 2, 2, 2);
        Rectangle r7 = new Rectangle(-10, 10, 1, 1);
        Rectangle r8 = new Rectangle(10, -10, 2, 3);
        Rectangle r9 = new Rectangle(-19, -10, 2, 3);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy6RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-5, -5, 5, 5);
        Rectangle r7 = new Rectangle(-11, -11, 1, 1);
        Rectangle r8 = new Rectangle(-20, -20, 2, 2);
        Rectangle r9 = new Rectangle(-30, -33, 2, 3);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy6RectsPartOverlapping(){
        Rectangle r1 = new Rectangle(0, 0, 6, 6);
        Rectangle r2 = new Rectangle(1, 1, 6, 6);
        Rectangle r3 = new Rectangle(2, 2, 6, 6);
        Rectangle r4 = new Rectangle(3, 3, 6, 6);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 6, 6);
        Rectangle r7 = new Rectangle(11, 11, 11, 11);
        Rectangle r8 = new Rectangle(32, 32, 2, 2);
        Rectangle r9 = new Rectangle(35, 35, 5, 5);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy6RectsPartOverlappingAndMixedCoordinate(){
        Rectangle r1 = new Rectangle(-3, -3, 9, 9);
        Rectangle r2 = new Rectangle(-2, -2, 9, 9);
        Rectangle r3 = new Rectangle(-1, -1, 9, 9);
        Rectangle r4 = new Rectangle(0, 0, 9, 9);
        Rectangle r5 = new Rectangle(1, 1, 9, 9);
        Rectangle r6 = new Rectangle(3, 3, 9, 9);
        Rectangle r7 = new Rectangle(-100, 100, 10, 10);
        Rectangle r8 = new Rectangle(100, -100, 10, 10);
        Rectangle r9 = new Rectangle(-100, -100, 10, 10);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy6RectsPartOverlappingAndNegativeCoordinate(){
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-11, -11, 10, 10);
        Rectangle r3 = new Rectangle(-12, -12, 10, 10);
        Rectangle r4 = new Rectangle(-13, -13, 10, 10);
        Rectangle r5 = new Rectangle(-14, -14, 10, 10);
        Rectangle r6 = new Rectangle(-15, -15, 10, 10);
        Rectangle r7 = new Rectangle(-20, -20, 2, 2);
        Rectangle r8 = new Rectangle(-30, -30, 3, 3);
        Rectangle r9 = new Rectangle(-100, -100, 8, 9);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy7RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(6, 6, 4, 4);
        Rectangle r8 = new Rectangle(12, 12, 3, 3);
        Rectangle r9 = new Rectangle(81, 18, 1, 1);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy7RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 9, 9);
        Rectangle r2 = new Rectangle(-2, -2, 8, 8);
        Rectangle r3 = new Rectangle(-1, -1, 7, 7);
        Rectangle r4 = new Rectangle(0, 0, 6, 6);
        Rectangle r5 = new Rectangle(1, 1, 5, 5);
        Rectangle r6 = new Rectangle(2, 2, 4, 4);
        Rectangle r7 = new Rectangle(3, 3, 3, 3);
        Rectangle r8 = new Rectangle(-100, 100, 10, 10);
        Rectangle r9 = new Rectangle(100, -100, 10, 10);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy7RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-5, -5, 5, 5);
        Rectangle r7 = new Rectangle(-4, -4, 4, 4);
        Rectangle r8 = new Rectangle(-12, -12, 2, 2);
        Rectangle r9 = new Rectangle(-15, -15, 2, 3);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy7RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 9, 9);
        Rectangle r4 = new Rectangle(3, 3, 9, 9);
        Rectangle r5 = new Rectangle(4, 4, 9, 9);
        Rectangle r6 = new Rectangle(5, 5, 9, 9);
        Rectangle r7 = new Rectangle(6, 6, 9, 9);
        Rectangle r8 = new Rectangle(16, 16, 4, 4);
        Rectangle r9 = new Rectangle(21, 21, 21, 21);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy7RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 13, 13);
        Rectangle r2 = new Rectangle(-2, -2, 13, 13);
        Rectangle r3 = new Rectangle(-1, -1, 13, 13);
        Rectangle r4 = new Rectangle(0, 0, 13, 13);
        Rectangle r5 = new Rectangle(1, 1, 13, 13);
        Rectangle r6 = new Rectangle(2, 2, 13, 13);
        Rectangle r7 = new Rectangle(3, 3, 13, 13);
        Rectangle r8 = new Rectangle(29, 29, 1, 1);
        Rectangle r9 = new Rectangle(-23, 23, 3, 6);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy7RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 9, 9);
        Rectangle r2 = new Rectangle(-11, -11, 9, 9);
        Rectangle r3 = new Rectangle(-12, -12, 9, 9);
        Rectangle r4 = new Rectangle(-13, -13, 9, 9);
        Rectangle r5 = new Rectangle(-14, -14, 9, 9);
        Rectangle r6 = new Rectangle(-15, -15, 9, 9);
        Rectangle r7 = new Rectangle(-16, -16, 9, 9);
        Rectangle r8 = new Rectangle(-22, -22, 2, 2);
        Rectangle r9 = new Rectangle(-32, -32, 1, 1);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));

    }

    @Test
    public void testIs9RectanglesOverlappingBy8RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(6, 6, 4, 4);
        Rectangle r8 = new Rectangle(7, 7, 3, 3);
        Rectangle r9 = new Rectangle(11, 11, 11, 11);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy8RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-3, -3, 8, 8);
        Rectangle r3 = new Rectangle(-1, -1, 6, 6);
        Rectangle r4 = new Rectangle(0, 0, 5, 5);
        Rectangle r5 = new Rectangle(1, 1, 4, 4);
        Rectangle r6 = new Rectangle(2, 2, 3, 3);
        Rectangle r7 = new Rectangle(3, 3, 2, 2);
        Rectangle r8 = new Rectangle(4, 4, 1, 1);
        Rectangle r9 = new Rectangle(10, -10, 1, 1);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy8RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-5, -5, 5, 5);
        Rectangle r7 = new Rectangle(-4, -4, 4, 4);
        Rectangle r8 = new Rectangle(-3, -3, 3, 3);
        Rectangle r9 = new Rectangle(-12, -13, 1, 2);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy8RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 9, 9);
        Rectangle r4 = new Rectangle(3, 3, 9, 9);
        Rectangle r5 = new Rectangle(4, 4, 9, 9);
        Rectangle r6 = new Rectangle(5, 5, 9, 9);
        Rectangle r7 = new Rectangle(6, 6, 9, 9);
        Rectangle r8 = new Rectangle(7, 7, 9, 9);
        Rectangle r9 = new Rectangle(17, 31, 3, 2);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy8RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-4, -4, 10, 10);
        Rectangle r3 = new Rectangle(-3, -3, 10, 10);
        Rectangle r4 = new Rectangle(-1, -1, 10, 10);
        Rectangle r5 = new Rectangle(0, 0, 11, 11);
        Rectangle r6 = new Rectangle(2, 2, 12, 12);
        Rectangle r7 = new Rectangle(3, 3, 13, 13);
        Rectangle r8 = new Rectangle(4, 4, 21, 20);
        Rectangle r9 = new Rectangle(-50, 50, 2, 3);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingBy8RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 9, 9);
        Rectangle r2 = new Rectangle(-11, -11, 9, 9);
        Rectangle r3 = new Rectangle(-12, -12, 9, 9);
        Rectangle r4 = new Rectangle(-13, -13, 9, 9);
        Rectangle r5 = new Rectangle(-14, -14, 9, 9);
        Rectangle r6 = new Rectangle(-15, -15, 9, 9);
        Rectangle r7 = new Rectangle(-16, -16, 9, 9);
        Rectangle r8 = new Rectangle(-17, -17, 9, 9);
        Rectangle r9 = new Rectangle(-20, -20, 2, 2);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingByNoOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 1, 1);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(3, 3, 3, 3);
        Rectangle r4 = new Rectangle(7, 7, 7, 7);
        Rectangle r5 = new Rectangle(15, 15, 1, 1);
        Rectangle r6 = new Rectangle(23, 23, 2, 2);
        Rectangle r7 = new Rectangle(32, 32, 2, 3);
        Rectangle r8 = new Rectangle(40, 44, 4, 1);
        Rectangle r9 = new Rectangle(55, 50, 3, 4);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(-1, 1, 1, 1);
        Rectangle r4 = new Rectangle(1, -1, 1, 1);
        Rectangle r5 = new Rectangle(3, 3, 3, 3);
        Rectangle r6 = new Rectangle(-3, 3, 1, 3);
        Rectangle r7 = new Rectangle(3, -3, 3, 1);
        Rectangle r8 = new Rectangle(-3, -3, 1, 1);
        Rectangle r9 = new Rectangle(10, -10, 2, 3);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs9RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(-2, -2, 1, 1);
        Rectangle r3 = new Rectangle(-3, -3, 1, 1);
        Rectangle r4 = new Rectangle(-4, -4, 1, 1);
        Rectangle r5 = new Rectangle(-5, -5, 1, 1);
        Rectangle r6 = new Rectangle(-6, -6, 1, 1);
        Rectangle r7 = new Rectangle(-7, -7, 1, 1);
        Rectangle r8 = new Rectangle(-8, -8, 1, 1);
        Rectangle r9 = new Rectangle(-9, -9, 1, 1);

        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r1));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r1, r2));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r5, r6, r7, r8, r9, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r6, r7, r8, r9, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r7, r8, r9, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r8, r9, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is9RectanglesOverlapping(r9, r1, r2, r3, r4, r5, r6, r7, r8));
    }

    @Test
    public void testIs10RectanglesOverlappingByFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(5, 5, 5, 5);
        Rectangle r7 = new Rectangle(6, 6, 4, 4);
        Rectangle r8 = new Rectangle(7, 7, 3, 3);
        Rectangle r9 = new Rectangle(8, 8, 2, 2);
        Rectangle r10 = new Rectangle(9, 9, 1, 1);

        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-4, -4, 9, 9);
        Rectangle r3 = new Rectangle(-3, -3, 8, 8);
        Rectangle r4 = new Rectangle(-2, -2, 7, 7);
        Rectangle r5 = new Rectangle(-1, -1, 6, 6);
        Rectangle r6 = new Rectangle(0, 0, 5, 5);
        Rectangle r7 = new Rectangle(1, 1, 4, 4);
        Rectangle r8 = new Rectangle(2, 2, 3, 3);
        Rectangle r9 = new Rectangle(3, 3, 2, 2);
        Rectangle r10 = new Rectangle(4, 4, 1, 1);

        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingByFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-5, -5, 5, 5);
        Rectangle r7 = new Rectangle(-4, -4, 4, 4);
        Rectangle r8 = new Rectangle(-3, -3, 3, 3);
        Rectangle r9 = new Rectangle(-2, -2, 2, 2);
        Rectangle r10 = new Rectangle(-1, -1, 1, 1);

        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingByPartOverlapping(){
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 10, 10);
        Rectangle r3 = new Rectangle(2, 2, 10, 10);
        Rectangle r4 = new Rectangle(3, 3, 10, 10);
        Rectangle r5 = new Rectangle(4, 4, 10, 10);
        Rectangle r6 = new Rectangle(5, 5, 10, 10);
        Rectangle r7 = new Rectangle(6, 6, 10, 10);
        Rectangle r8 = new Rectangle(7, 7, 10, 10);
        Rectangle r9 = new Rectangle(8, 8, 10, 10);
        Rectangle r10 = new Rectangle(9, 9, 10, 10);

        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingByPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-4, -4, 10, 10);
        Rectangle r3 = new Rectangle(-3, -3, 10, 10);
        Rectangle r4 = new Rectangle(-2, -2, 10, 10);
        Rectangle r5 = new Rectangle(-1, -1, 10, 10);
        Rectangle r6 = new Rectangle(0, 0, 10, 10);
        Rectangle r7 = new Rectangle(1, 1, 10, 10);
        Rectangle r8 = new Rectangle(2, 2, 10, 10);
        Rectangle r9 = new Rectangle(3, 3, 10, 10);
        Rectangle r10 = new Rectangle(4, 4, 10,10);

        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingByPartOverlappingAndNegativeCoordinate(){
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-11, -11, 10, 10);
        Rectangle r3 = new Rectangle(-12, -12, 10, 10);
        Rectangle r4 = new Rectangle(-13, -13, 10, 10);
        Rectangle r5 = new Rectangle(-14, -14, 10, 10);
        Rectangle r6 = new Rectangle(-15, -15, 10, 10);
        Rectangle r7 = new Rectangle(-16, -16, 10, 10);
        Rectangle r8 = new Rectangle(-17, -17, 10, 10);
        Rectangle r9 = new Rectangle(-18, -18, 10, 10);
        Rectangle r10 = new Rectangle(-19, -19, 10, 10);

        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertTrue(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy2RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 2, 2);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(4, 4, 1, 1);
        Rectangle r4 = new Rectangle(6, 6, 5, 5);
        Rectangle r5 = new Rectangle(12, 12, 2, 2);
        Rectangle r6 = new Rectangle(15, 15, 2, 2);
        Rectangle r7 = new Rectangle(21, 21, 2, 2);
        Rectangle r8 = new Rectangle(30, 30, 1, 2);
        Rectangle r9 = new Rectangle(35, 35, 5, 5);
        Rectangle r10 = new Rectangle(40, 40, 4, 4);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy2RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 4, 4);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(-5, -5, 1, 1);
        Rectangle r4 = new Rectangle(5, -5, 1, 1);
        Rectangle r5 = new Rectangle(-5, -5, 1, 1);
        Rectangle r6 = new Rectangle(5, 5, 1, 1);
        Rectangle r7 = new Rectangle(9, 9, 2, 2);
        Rectangle r8 = new Rectangle(-9, -9, 2, 2);
        Rectangle r9 = new Rectangle(9, -9, 2, 2);
        Rectangle r10 = new Rectangle(10, 10, 2, 3);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy2RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 2, 2);
        Rectangle r2 = new Rectangle(-1, -1, 1, 1);
        Rectangle r3 = new Rectangle(-5, -5, 1, 1);
        Rectangle r4 = new Rectangle(-8, -8, 2, 2);
        Rectangle r5 = new Rectangle(-12, -12, 1, 1);
        Rectangle r6 = new Rectangle(-20, -20, 1, 1);
        Rectangle r7 = new Rectangle(-22, -22, 2, 2);
        Rectangle r8 = new Rectangle(-35, -35, 3, 3);
        Rectangle r9 = new Rectangle(-40, -40, 4, 4);
        Rectangle r10 = new Rectangle(-55, -55, 5, 5);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy2RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 2);
        Rectangle r2 = new Rectangle(2, 0, 12, 3);
        Rectangle r3 = new Rectangle(20, 4, 5, 21);
        Rectangle r4 = new Rectangle(30, 30, 5, 5);
        Rectangle r5 = new Rectangle(40, 40, 8, 8);
        Rectangle r6 = new Rectangle(50, 50, 2, 2);
        Rectangle r7 = new Rectangle(53, 53, 7, 7);
        Rectangle r8 = new Rectangle(61, 61, 4, 4);
        Rectangle r9 = new Rectangle(70, 70, 7, 7);
        Rectangle r10 = new Rectangle(80, 80, 10, 10);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy2RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 2, 2);
        Rectangle r2 = new Rectangle(1, 1, 1, 1);
        Rectangle r3 = new Rectangle(5, -5, 1, 1);
        Rectangle r4 = new Rectangle(-5, 5, 1, 1);
        Rectangle r5 = new Rectangle(-5, -5, 1, 1);
        Rectangle r6 = new Rectangle(5, 5, 1, 1);
        Rectangle r7 = new Rectangle(-9, 9, 2, 2);
        Rectangle r8 = new Rectangle(9, -9, 1, 1);
        Rectangle r9 = new Rectangle(9, 9, 2, 3);
        Rectangle r10 = new Rectangle(-9, -9, 1, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy2RectsPartOverlappingAndNegativeCoordinate(){
        Rectangle r1 = new Rectangle(-10, -10, 3, 3);
        Rectangle r2 = new Rectangle(-9, -9, 1, 1);
        Rectangle r3 = new Rectangle(-6, -6, 1, 1);
        Rectangle r4 = new Rectangle(-4, -4, 1, 1);
        Rectangle r5 = new Rectangle(-2, -2, 1, 1);
        Rectangle r6 = new Rectangle(-12, -12, 1, 1);
        Rectangle r7 = new Rectangle(-15, -15, 2, 2);
        Rectangle r8 = new Rectangle(-20, -20, 2, 2);
        Rectangle r9 = new Rectangle(-30, -30, 2, 2);
        Rectangle r10 = new Rectangle(-50, -40, 1, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy3RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 4, 4);
        Rectangle r2 = new Rectangle(1, 1, 3, 3);
        Rectangle r3 = new Rectangle(2, 2, 1, 1);
        Rectangle r4 = new Rectangle(5, 5, 1, 1);
        Rectangle r5 = new Rectangle(6, 6, 1, 1);
        Rectangle r6 = new Rectangle(7, 7, 1, 1);
        Rectangle r7 = new Rectangle(8, 8, 1, 1);
        Rectangle r8 = new Rectangle(9, 9, 1, 1);
        Rectangle r9 = new Rectangle(11, 11, 1, 1);
        Rectangle r10 = new Rectangle(12, 12, 1, 1);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy3RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 6, 6);
        Rectangle r2 = new Rectangle(0, 0, 3, 3);
        Rectangle r3 = new Rectangle(1, 1, 2, 2);
        Rectangle r4 = new Rectangle(-5, -5, 1, 1);
        Rectangle r5 = new Rectangle(5, 5, 1, 1);
        Rectangle r6 = new Rectangle(5, -5, 1, 1);
        Rectangle r7 = new Rectangle(-5, 5, 1, 1);
        Rectangle r8 = new Rectangle(-9, -9, 2, 2);
        Rectangle r9 = new Rectangle(9, 9, 2, 2);
        Rectangle r10 = new Rectangle(10, -10, 1, 1);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy3RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-4, -4, 4, 4);
        Rectangle r2 = new Rectangle(-3, -3, 3, 3);
        Rectangle r3 = new Rectangle(-2, -2, 2, 2);
        Rectangle r4 = new Rectangle(-1, -1, 1, 1);
        Rectangle r5 = new Rectangle(-6, -6, 1, 1);
        Rectangle r6 = new Rectangle(-8, -8, 1, 1);
        Rectangle r7 = new Rectangle(-19, -19, 7, 7);
        Rectangle r8 = new Rectangle(-21, -21, 2, 2);
        Rectangle r9 = new Rectangle(-30, -30, 1, 1);
        Rectangle r10 = new Rectangle(-45, -45, 2, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy3RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 4, 4);
        Rectangle r2 = new Rectangle(0, 1, 5, 4);
        Rectangle r3 = new Rectangle(1, 0, 5, 6);
        Rectangle r4 = new Rectangle(6, 6, 2, 2);
        Rectangle r5 = new Rectangle(8, 8, 3, 3);
        Rectangle r6 = new Rectangle(11, 11, 3, 3);
        Rectangle r7 = new Rectangle(15, 15, 2, 2);
        Rectangle r8 = new Rectangle(18, 18, 4, 4);
        Rectangle r9 = new Rectangle(22, 22, 4, 4);
        Rectangle r10 = new Rectangle(30, 30, 30, 3);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy3RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 6, 6);
        Rectangle r2 = new Rectangle(0, 0, 4, 4);
        Rectangle r3 = new Rectangle(1, 1, 6, 6);
        Rectangle r4 = new Rectangle(-9, 9, 1, 1);
        Rectangle r5 = new Rectangle(9, -9, 1, 1);
        Rectangle r6 = new Rectangle(-9, -9, 1, 1);
        Rectangle r7 = new Rectangle(9, 9, 1, 1);
        Rectangle r8 = new Rectangle(-20, 20, 2, 2);
        Rectangle r9 = new Rectangle(-20, -20, 8, 2);
        Rectangle r10 = new Rectangle(-50, 120, 23, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy3RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 5, 5);
        Rectangle r2 = new Rectangle(-4, -4, 4, 4);
        Rectangle r3 = new Rectangle(-3, -3, 3, 3);
        Rectangle r4 = new Rectangle(-10, -10, 1, 1);
        Rectangle r5 = new Rectangle(-14, -14, 2, 2);
        Rectangle r6 = new Rectangle(-18, -18, 1, 1);
        Rectangle r7 = new Rectangle(-22, -22, 1, 1);
        Rectangle r8 = new Rectangle(-30, -30, 2, 2);
        Rectangle r9 = new Rectangle(-45, -40, 2, 5);
        Rectangle r10 = new Rectangle(-65, -65, 5, 5);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy4RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 6, 6);
        Rectangle r2 = new Rectangle(1, 1, 5, 5);
        Rectangle r3 = new Rectangle(2, 2, 4, 4);
        Rectangle r4 = new Rectangle(3, 3, 1, 1);
        Rectangle r5 = new Rectangle(8, 8, 2, 2);
        Rectangle r6 = new Rectangle(11, 11, 2, 2);
        Rectangle r7 = new Rectangle(14, 14, 15, 15);
        Rectangle r8 = new Rectangle(31, 31, 3, 4);
        Rectangle r9 = new Rectangle(43, 43, 4, 5);
        Rectangle r10 = new Rectangle(50, 50, 55, 55);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy4RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-3, -3, 6, 6);
        Rectangle r2 = new Rectangle(-2, -2, 5, 5);
        Rectangle r3 = new Rectangle(0, 0, 3, 3);
        Rectangle r4 = new Rectangle(1, 1, 1, 1);
        Rectangle r5 = new Rectangle(-10, 10, 1, 1);
        Rectangle r6 = new Rectangle(10, -10, 1, 1);
        Rectangle r7 = new Rectangle(10, 10, 1, 1);
        Rectangle r8 = new Rectangle(-10, -10, 1, 1);
        Rectangle r9 = new Rectangle(-123, -123, 13, 13);
        Rectangle r10 = new Rectangle(123, 123, 123, 123);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy4RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-20, -20, 2, 2);
        Rectangle r6 = new Rectangle(-22, -22, 1, 1);
        Rectangle r7 = new Rectangle(-32, -32, 3, 3);
        Rectangle r8 = new Rectangle(-35, -35, 1, 1);
        Rectangle r9 = new Rectangle(-44, -44, 4, 4);
        Rectangle r10 = new Rectangle(-55, -55, 3, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy4RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(1, 1, 10, 10);
        Rectangle r3 = new Rectangle(2, 2, 10, 10);
        Rectangle r4 = new Rectangle(3, 3, 10, 10);
        Rectangle r5 = new Rectangle(14, 14, 1, 1);
        Rectangle r6 = new Rectangle(15, 15, 3, 3);
        Rectangle r7 = new Rectangle(21, 21, 1, 1);
        Rectangle r8 = new Rectangle(22, 22, 2, 2);
        Rectangle r9 = new Rectangle(32, 32, 2, 2);
        Rectangle r10 = new Rectangle(44, 44, 4, 4);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy4RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 8, 8);
        Rectangle r2 = new Rectangle(-1, -1, 8, 8);
        Rectangle r3 = new Rectangle(0, 0, 9, 9);
        Rectangle r4 = new Rectangle(1, 1, 10, 10);
        Rectangle r5 = new Rectangle(-10, -10, 1, 1);
        Rectangle r6 = new Rectangle(-10, 10, 2, 2);
        Rectangle r7 = new Rectangle(14, 14, 2, 2);
        Rectangle r8 = new Rectangle(20, -34, 2, 2);
        Rectangle r9 = new Rectangle(-100, -100, 2, 2);
        Rectangle r10 = new Rectangle(100, 100, 2, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy4RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-9, -9, 9, 9);
        Rectangle r2 = new Rectangle(-10, -10, 9, 9);
        Rectangle r3 = new Rectangle(-11, -11, 9, 9);
        Rectangle r4 = new Rectangle(-12, -12, 9, 9);
        Rectangle r5 = new Rectangle(-13, -13, 1, 1);
        Rectangle r6 = new Rectangle(-21, -21, 2, 2);
        Rectangle r7 = new Rectangle(-23, -23, 1, 1);
        Rectangle r8 = new Rectangle(-34, -34, 4, 4);
        Rectangle r9 = new Rectangle(-54, -54, 4, 4);
        Rectangle r10 = new Rectangle(-1024, -1024, 8, 8);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy5RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 6, 6);
        Rectangle r2 = new Rectangle(1, 1, 5, 5);
        Rectangle r3 = new Rectangle(2, 2, 4, 4);
        Rectangle r4 = new Rectangle(3, 3, 3, 3);
        Rectangle r5 = new Rectangle(4, 4, 2, 2);
        Rectangle r6 = new Rectangle(7, 7, 3, 3);
        Rectangle r7 = new Rectangle(11, 11, 9, 9);
        Rectangle r8 = new Rectangle(21, 21, 2, 2);
        Rectangle r9 = new Rectangle(24, 24, 4, 4);
        Rectangle r10 = new Rectangle(28, 28, 2, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy5RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 6, 6);
        Rectangle r2 = new Rectangle(-1, -1, 5, 5);
        Rectangle r3 = new Rectangle(0, 0, 4, 4);
        Rectangle r4 = new Rectangle(1, 1, 3, 3);
        Rectangle r5 = new Rectangle(2, 2, 2, 2);
        Rectangle r6 = new Rectangle(5, 5, 1, 1);
        Rectangle r7 = new Rectangle(-5, -5, 1, 1);
        Rectangle r8 = new Rectangle(-10, 10, 2, 3);
        Rectangle r9 = new Rectangle(10, 10, 3, 3);
        Rectangle r10 = new Rectangle(-10, -10, 2, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy5RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-9, -9, 9, 9);
        Rectangle r2 = new Rectangle(-8, -8, 8, 8);
        Rectangle r3 = new Rectangle(-7, -7, 7, 7);
        Rectangle r4 = new Rectangle(-6, -6, 6, 6);
        Rectangle r5 = new Rectangle(-5, -5, 5, 5);
        Rectangle r6 = new Rectangle(-11, -11, 1, 1);
        Rectangle r7 = new Rectangle(-21, -21, 1, 1);
        Rectangle r8 = new Rectangle(-22, -22, 1, 1);
        Rectangle r9 = new Rectangle(-15, -15, 2, 2);
        Rectangle r10 = new Rectangle(-33, -33, 3, 3);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy5RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 6, 6);
        Rectangle r2 = new Rectangle(1, 1, 6, 6);
        Rectangle r3 = new Rectangle(2, 2, 6, 6);
        Rectangle r4 = new Rectangle(3, 3, 6, 6);
        Rectangle r5 = new Rectangle(4, 4, 6, 6);
        Rectangle r6 = new Rectangle(12, 12, 3, 3);
        Rectangle r7 = new Rectangle(15, 18, 5, 2);
        Rectangle r8 = new Rectangle(25, 31, 10, 4);
        Rectangle r9 = new Rectangle(44, 44, 4, 4);
        Rectangle r10 = new Rectangle(51, 52, 52, 51);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy5RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, -2, 6, 6);
        Rectangle r2 = new Rectangle(-1, -1, 6, 6);
        Rectangle r3 = new Rectangle(0, 0, 6, 6);
        Rectangle r4 = new Rectangle(1, 1, 6, 6);
        Rectangle r5 = new Rectangle(2, 2, 6, 6);
        Rectangle r6 = new Rectangle(-10, 10, 1, 1);
        Rectangle r7 = new Rectangle(10, -10, 1, 1);
        Rectangle r8 = new Rectangle(10, 10, 1, 1);
        Rectangle r9 = new Rectangle(-10, -10, 2, 1);
        Rectangle r10 = new Rectangle(1024, 1024, 2, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy5RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 5, 5);
        Rectangle r2 = new Rectangle(-6, -6, 5, 5);
        Rectangle r3 = new Rectangle(-7, -7, 5, 5);
        Rectangle r4 = new Rectangle(-8, -8, 5, 5);
        Rectangle r5 = new Rectangle(-9, -9, 5, 5);
        Rectangle r6 = new Rectangle(-10, -10, 1, 1);
        Rectangle r7 = new Rectangle(-123, -123, 23, 23);
        Rectangle r8 = new Rectangle(-100, -100, 10, 10);
        Rectangle r9 = new Rectangle(-80, -80, 20, 20);
        Rectangle r10 = new Rectangle(-1024, -1024, 24, 24);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy6RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(2, 2, 7, 7);
        Rectangle r4 = new Rectangle(3, 3, 6, 6);
        Rectangle r5 = new Rectangle(4, 4, 5, 5);
        Rectangle r6 = new Rectangle(5, 5, 4, 4);
        Rectangle r7 = new Rectangle(11, 11, 1, 1);
        Rectangle r8 = new Rectangle(13, 13, 13, 13);
        Rectangle r9 = new Rectangle(32, 32, 2, 2);
        Rectangle r10 = new Rectangle(35, 36, 5, 4);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy6RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-4, -4, 8, 8);
        Rectangle r2 = new Rectangle(-3, -3, 7, 7);
        Rectangle r3 = new Rectangle(-1, -1, 5, 5);
        Rectangle r4 = new Rectangle(0, 0, 4, 4);
        Rectangle r5 = new Rectangle(1, 1, 3, 3);
        Rectangle r6 = new Rectangle(2, 2, 2, 2);
        Rectangle r7 = new Rectangle(-10, 10, 1, 1);
        Rectangle r8 = new Rectangle(10, -10, 1, 1);
        Rectangle r9 = new Rectangle(10,10, 1, 1);
        Rectangle r10 = new Rectangle(-10, -10, 1, 1);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy6RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-9, -9, 9, 9);
        Rectangle r2 = new Rectangle(-8, -8, 8, 8);
        Rectangle r3 = new Rectangle(-7, -7, 7, 7);
        Rectangle r4 = new Rectangle(-6, -6, 6, 6);
        Rectangle r5 = new Rectangle(-5, -5, 5, 5);
        Rectangle r6 = new Rectangle(-4, -4, 4, 4);
        Rectangle r7 = new Rectangle(-10, -10, 1, 1);
        Rectangle r8 = new Rectangle(-100, -100, 10, 10);
        Rectangle r9 = new Rectangle(-1000, -1000, 100, 100);
        Rectangle r10 = new Rectangle(-10000, -10000, -1000, -1000);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy6RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 7, 7);
        Rectangle r2 = new Rectangle(1, 1, 7, 7);
        Rectangle r3 = new Rectangle(2, 2, 7, 7);
        Rectangle r4 = new Rectangle(3, 3, 7, 7);
        Rectangle r5 = new Rectangle(4, 4, 7, 7);
        Rectangle r6 = new Rectangle(5, 5, 7, 7);
        Rectangle r7 = new Rectangle(8, 8, 2, 2);
        Rectangle r8 = new Rectangle(11, 11, 21, 21);
        Rectangle r9 = new Rectangle(40, 40, 1, 1);
        Rectangle r10 = new Rectangle(42, 43, 8, 7);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy6RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-4, -4, 8, 8);
        Rectangle r2 = new Rectangle(-3, -3, 8, 8);
        Rectangle r3 = new Rectangle(-1, -1, 8, 8);
        Rectangle r4 = new Rectangle(0, 0, 9, 9);
        Rectangle r5 = new Rectangle(1, 1, 9, 9);
        Rectangle r6 = new Rectangle(2, 2, 9, 9);
        Rectangle r7 = new Rectangle(-12, 12, 2, 2);
        Rectangle r8 = new Rectangle(-12, -12, 2, 2);
        Rectangle r9 = new Rectangle(12, 12, 2, 2);
        Rectangle r10 = new Rectangle(-12, -12, 2, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy6RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-9, -9, 9, 9);
        Rectangle r2 = new Rectangle(-8, -8, 9, 9);
        Rectangle r3 = new Rectangle(-7, -7, 9, 9);
        Rectangle r4 = new Rectangle(-6, -6, 9, 9);
        Rectangle r5 = new Rectangle(-5, -5, 9, 9);
        Rectangle r6 = new Rectangle(-4, -4, 9, 9);
        Rectangle r7 = new Rectangle(-10, -10, 1, 1);
        Rectangle r8 = new Rectangle(-12, -12, 2, 2);
        Rectangle r9 = new Rectangle(-22, -22, 4, 4);
        Rectangle r10 = new Rectangle(-25, -25, 1, 1);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy7RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(2, 2, 7, 7);
        Rectangle r4 = new Rectangle(3, 3, 6, 6);
        Rectangle r5 = new Rectangle(4, 4, 5, 5);
        Rectangle r6 = new Rectangle(5, 5, 4, 4);
        Rectangle r7 = new Rectangle(6, 6, 3, 3);
        Rectangle r8 = new Rectangle(10, 10, 12, 12);
        Rectangle r9 = new Rectangle(24, 24, 34, 34);
        Rectangle r10 = new Rectangle(66, 77, 66, 77);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy7RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-4, -4, 8, 8);
        Rectangle r2 = new Rectangle(-3, -3, 7, 7);
        Rectangle r3 = new Rectangle(-2, -2, 6, 6);
        Rectangle r4 = new Rectangle(-1, -1, 5, 5);
        Rectangle r5 = new Rectangle(0,0, 4, 4);
        Rectangle r6 = new Rectangle(1, 1, 3, 3);
        Rectangle r7 = new Rectangle(2, 2, 2, 2);
        Rectangle r8 = new Rectangle(-50, 50, 50, 5);
        Rectangle r9 = new Rectangle(-12, -22, 2, 2);
        Rectangle r10 = new Rectangle(100, -88, 2, 3);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy7RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-5, -5, 5, 5);
        Rectangle r7 = new Rectangle(-4, -4, 4, 4);
        Rectangle r8 = new Rectangle(-11, -11, 1, 1);
        Rectangle r9 = new Rectangle(-23, -23, 3, 3);
        Rectangle r10 = new Rectangle(-45, -123, 3, 6);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy7RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 8, 8);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(2, 2, 8, 8);
        Rectangle r4 = new Rectangle(3, 3, 8, 8);
        Rectangle r5 = new Rectangle(4, 4, 8, 8);
        Rectangle r6 = new Rectangle(5, 5, 8, 8);
        Rectangle r7 = new Rectangle(6, 6, 8, 8);
        Rectangle r8 = new Rectangle(15, 21, 10, 4);
        Rectangle r9 = new Rectangle(44, 44, 44, 44);
        Rectangle r10 = new Rectangle(88, 89, 22, 21);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy7RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-4, -4, 9, 9);
        Rectangle r2 = new Rectangle(-3, -3, 9, 9);
        Rectangle r3 = new Rectangle(-2, -2, 9, 9);
        Rectangle r4 = new Rectangle(0, 0, 9, 9);
        Rectangle r5 = new Rectangle(2, 2, 9, 9);
        Rectangle r6 = new Rectangle(1, 1, 9, 9);
        Rectangle r7 = new Rectangle(3, 3, 9, 9);
        Rectangle r8 = new Rectangle(-124, 124, 26, 2);
        Rectangle r9 = new Rectangle(-324, 324, 2, 3);
        Rectangle r10 = new Rectangle(-211, -455, 12,32);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy7RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-8, -8, 8, 8);
        Rectangle r2 = new Rectangle(-9, -9, 8, 8);
        Rectangle r3 = new Rectangle(-10, -10, 8, 8);
        Rectangle r4 = new Rectangle(-11, -11, 8, 8);
        Rectangle r5 = new Rectangle(-12, -12, 8, 8);
        Rectangle r6 = new Rectangle(-13, -13, 8, 8);
        Rectangle r7 = new Rectangle(-14, -14, 8, 8);
        Rectangle r8 = new Rectangle(-20, -20, 2, 2);
        Rectangle r9 = new Rectangle(-25, 25, 1, 1);
        Rectangle r10 = new Rectangle(-66, -55, 16, 5);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy8RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(2, 2, 7, 7);
        Rectangle r4 = new Rectangle(3, 3, 6, 6);
        Rectangle r5 = new Rectangle(4, 4, 5, 5);
        Rectangle r6 = new Rectangle(5, 5, 4, 4);
        Rectangle r7 = new Rectangle(6, 6, 3, 3);
        Rectangle r8 = new Rectangle(7, 7, 2, 2);
        Rectangle r9 = new Rectangle(10, 10, 10, 10);
        Rectangle r10 = new Rectangle(20, 20, 22, 32);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy8RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-4, -4, 9, 9);
        Rectangle r3 = new Rectangle(-3, -3, 8, 8);
        Rectangle r4 = new Rectangle(-2, -2, 7, 7);
        Rectangle r5 = new Rectangle(0, 0, 5, 5);
        Rectangle r6 = new Rectangle(1, 1, 4, 4);
        Rectangle r7 = new Rectangle(2, 2, 3, 3);
        Rectangle r8 = new Rectangle(2, 2, 2, 2);
        Rectangle r9 = new Rectangle(-100, 100, 1, 10);
        Rectangle r10 = new Rectangle(100, -100, 10, 1);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy8RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 10, 10);
        Rectangle r2 = new Rectangle(-9, -9, 9, 9);
        Rectangle r3 = new Rectangle(-8, -8, 8, 8);
        Rectangle r4 = new Rectangle(-7, -7, 7, 7);
        Rectangle r5 = new Rectangle(-6, -6, 6, 6);
        Rectangle r6 = new Rectangle(-5, -5, 5, 5);
        Rectangle r7 = new Rectangle(-4, -4, 4, 4);
        Rectangle r8 = new Rectangle(-3, -3, 3, 3);
        Rectangle r9 = new Rectangle(-20, -20, 2, 2);
        Rectangle r10 = new Rectangle(-32, -32, 3, 4);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy8RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 9, 9);
        Rectangle r4 = new Rectangle(3, 3, 9, 9);
        Rectangle r5 = new Rectangle(4, 4, 9, 9);
        Rectangle r6 = new Rectangle(5, 5, 9, 9);
        Rectangle r7 = new Rectangle(6, 6, 9, 9);
        Rectangle r8 = new Rectangle(7, 7, 9, 9);
        Rectangle r9 = new Rectangle(20, 20, 2, 2);
        Rectangle r10 = new Rectangle(23, 23, 23, 23);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy8RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-5, -5, 10, 10);
        Rectangle r2 = new Rectangle(-4, -4, 10, 10);
        Rectangle r3 = new Rectangle(-3, -3, 10, 10);
        Rectangle r4 = new Rectangle(-2, -2, 10, 10);
        Rectangle r5 = new Rectangle(0, 0, 10, 10);
        Rectangle r6 = new Rectangle(1, 1, 10, 10);
        Rectangle r7 = new Rectangle(2, 2, 10, 10);
        Rectangle r8 = new Rectangle(3, 3, 10, 10);
        Rectangle r9 = new Rectangle(14, 14, 3, 3);
        Rectangle r10 = new Rectangle(-20, -123, 23, 12);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy8RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-9, -9, 9, 9);
        Rectangle r2 = new Rectangle(-8, -8, 9, 9);
        Rectangle r3 = new Rectangle(-7, -7, 9, 9);
        Rectangle r4 = new Rectangle(-6, -6, 9, 9);
        Rectangle r5 = new Rectangle(-5, -5, 9, 9);
        Rectangle r6 = new Rectangle(-4, -4, 9, 9);
        Rectangle r7 = new Rectangle(-3, -3, 9, 9);
        Rectangle r8 = new Rectangle(-2, -2, 9, 9);
        Rectangle r9 = new Rectangle(-10, -10, 1, 1);
        Rectangle r10 = new Rectangle(-15, -16, 5, 6);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy9RectsFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 8, 8);
        Rectangle r3 = new Rectangle(2, 2, 7, 7);
        Rectangle r4 = new Rectangle(3, 3, 6, 6);
        Rectangle r5 = new Rectangle(4, 4, 5, 5);
        Rectangle r6 = new Rectangle(5, 5, 4, 4);
        Rectangle r7 = new Rectangle(6, 6, 3, 3);
        Rectangle r8 = new Rectangle(7, 7, 2, 2);
        Rectangle r9 = new Rectangle(8, 8, 1, 1);
        Rectangle r10 = new Rectangle(10, 10, 10, 10);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy9RectsFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-6, -6, 12, 12);
        Rectangle r2 = new Rectangle(-5, -5, 11, 11);
        Rectangle r3 = new Rectangle(-4, -4, 10, 10);
        Rectangle r4 = new Rectangle(-3, -3, 9,9);
        Rectangle r5 = new Rectangle(-2, -2, 8, 8);
        Rectangle r6 = new Rectangle(0, 0, 6, 6);
        Rectangle r7 = new Rectangle(1, 1, 5, 5);
        Rectangle r8 = new Rectangle(2, 2, 4, 4);
        Rectangle r9 = new Rectangle(4, 4, 1, 1);
        Rectangle r10 = new Rectangle(-123, 123, 23, 7);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy9RectsFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-9, -9, 9, 9);
        Rectangle r2 = new Rectangle(-8, -8, 8, 8);
        Rectangle r3 = new Rectangle(-7, -7, 7, 7);
        Rectangle r4 = new Rectangle(-6, -6, 6, 6);
        Rectangle r5 = new Rectangle(-5, -5, 5, 5);
        Rectangle r6 = new Rectangle(-4, -4, 4, 4);
        Rectangle r7 = new Rectangle(-3, -3, 3, 3);
        Rectangle r8 = new Rectangle(-2, -2, 2, 2);
        Rectangle r9 = new Rectangle(-1, -1, 1, 1);
        Rectangle r10 = new Rectangle(-10, -10, 1, 1);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy9RectsPartOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 9, 9);
        Rectangle r2 = new Rectangle(1, 1, 9, 9);
        Rectangle r3 = new Rectangle(2, 2, 9, 9);
        Rectangle r4 = new Rectangle(3, 3, 9, 9);
        Rectangle r5 = new Rectangle(4, 4, 9, 9);
        Rectangle r6 = new Rectangle(5, 5, 9, 9);
        Rectangle r7 = new Rectangle(6, 6, 9, 9);
        Rectangle r8 = new Rectangle(7, 7, 9, 9);
        Rectangle r9 = new Rectangle(8, 8, 9, 9);
        Rectangle r10 = new Rectangle(20, 20, 2, 2);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy9RectsPartOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-9, -9, 9, 9);
        Rectangle r2 = new Rectangle(-8, -8, 9, 9);
        Rectangle r3 = new Rectangle(-7, -7, 9, 9);
        Rectangle r4 = new Rectangle(-5, -5, 9, 9);
        Rectangle r5 = new Rectangle(-1, -2, 9, 9);
        Rectangle r6 = new Rectangle(0, 0, 10, 10);
        Rectangle r7 = new Rectangle(1, 4, 9, 9);
        Rectangle r8 = new Rectangle(2, 3, 9, 9);
        Rectangle r9 = new Rectangle(0, 0, 15, 15);
        Rectangle r10 = new Rectangle(-100, 100, 24, 42);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingBy9RectsPartOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-20, -20, 20, 20);
        Rectangle r2 = new Rectangle(-19, -19, 20, 20);
        Rectangle r3 = new Rectangle(-18, -18, 20, 20);
        Rectangle r4 = new Rectangle(-17, -17, 20, 20);
        Rectangle r5 = new Rectangle(-15, -15, 20, 20);
        Rectangle r6 = new Rectangle(-12, -12, 12, 12);
        Rectangle r7 = new Rectangle(-22, -22, 21, 21);
        Rectangle r8 = new Rectangle(-23, -23, 21, 21);
        Rectangle r9 = new Rectangle(-25, -25, 22, 22);
        Rectangle r10 = new Rectangle(-123, -123, 3, 3);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingByNoOverlapping(){
        Rectangle r1 = new Rectangle(0, 0, 1, 1);
        Rectangle r2 = new Rectangle(2, 2, 2, 2);
        Rectangle r3 = new Rectangle(5, 5, 5, 5);
        Rectangle r4 = new Rectangle(12, 12, 4, 4);
        Rectangle r5 = new Rectangle(18, 17, 2, 3);
        Rectangle r6 = new Rectangle(20, 20, 5, 5);
        Rectangle r7 = new Rectangle(26, 26, 4, 4);
        Rectangle r8 = new Rectangle(32, 32, 8, 8);
        Rectangle r9 = new Rectangle(44, 44, 44, 44);
        Rectangle r10 = new Rectangle(111, 111, 111, 111);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingByNoOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, -1, 1, 1);
        Rectangle r2 = new Rectangle(1, -1, 1, 1);
        Rectangle r3 = new Rectangle(-1, 1, 1, 1);
        Rectangle r4 = new Rectangle(1, 1, 1, 1);
        Rectangle r5 = new Rectangle(10, 11, 2, 1);
        Rectangle r6 = new Rectangle(-123, 345, 2, 2);
        Rectangle r7 = new Rectangle(123, -234, 2, 2);
        Rectangle r8 = new Rectangle(-234, 123, 4, 3);
        Rectangle r9 = new Rectangle(-1000, -1000, 100, 100);
        Rectangle r10 = new Rectangle(1000, 1000, 100, 100);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

    @Test
    public  void restIs10RectanglesOverlappingByNoOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-1000, -1000, 1, 1);
        Rectangle r2 = new Rectangle(-900, -900, 2, 2);
        Rectangle r3 = new Rectangle(-800, -800, 3, 3);
        Rectangle r4 = new Rectangle(-700, -700, 4, 4);
        Rectangle r5 = new Rectangle(-600, -600, 5, 5);
        Rectangle r6 = new Rectangle(-500, -500, 6, 6);
        Rectangle r7 = new Rectangle(-400, -400, 7, 7);
        Rectangle r8 = new Rectangle(-300, -300, 8, 8);
        Rectangle r9 = new Rectangle(-200, -200, 9, 9);
        Rectangle r10 = new Rectangle(-100, -100, 10, 10);

        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r2, r3, r4, r5, r6, r7, r8, r9, r10, r1));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r3, r4, r5, r6, r7, r8, r9, r10, r1, r2));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r4, r5, r6, r7, r8, r9, r10, r1, r2, r3));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r5, r6, r7, r8, r9, r10, r1, r2, r3, r4));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r6, r7, r8, r9, r10, r1, r2, r3, r4, r5));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r7, r8, r9, r10, r1, r2, r3, r4, r5, r6));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r8, r9, r10, r1, r2, r3, r4, r5, r6, r7));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r9, r10, r1, r2, r3, r4, r5, r6, r7, r8));
        assertFalse(RectanglesOverlapping.is10RectanglesOverlapping(r10, r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }
    //***********************************************************************
    @Test
    public void testFound2RectanglesOverlappingByFullOverlapping() {
        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(0, 0, 5, 5);
        Rectangle overlapR1R2 = new Rectangle(0, 0, 5, 5);
        assertEquals(overlapR1R2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapR1R2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testFound2RectanglesOverlappingByFullOverlappingAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-10, 1, 20, 20);
        Rectangle r2 = new Rectangle(-9, 1, 10, 10);
        Rectangle overlapr1r2 = new Rectangle(-9, 1, 10, 10);
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testFound2RectanglesOverlappingByFullOverlappingAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-10, -10, 20, 20);
        Rectangle r2 = new Rectangle(-5, -5, 10, 10);
        Rectangle overlapr1r2 = new Rectangle(-5, -5, 10, 10);
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testFound2RectanglesOverlappingByPartOverlappingTopRightBottomLeft() {
        Rectangle r1 = new Rectangle(5, 5, 5, 5);
        Rectangle r2 = new Rectangle(9, 9, 5, 5);
        Rectangle overlapr1r2 = new Rectangle(9, 9, 1, 1);
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testFound2RectanglesOverlappingByPartOverlappingTopRightBottomLeftAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-1, 1, 5, 5);
        Rectangle r2 = new Rectangle(-2, 0, 5, 5);
        Rectangle overlapr1r2 = new Rectangle(-1, 1, 4, 4);
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testFound2RectanglesOverlappingByPartOverlappingTopRightBottomLeftAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-100, -100, 50, 50);
        Rectangle r2 = new Rectangle(-150, -150, 60, 70);
        Rectangle overlapr1r2 = new Rectangle(-100, -100, 20, 10);
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testFound2RectanglesOverlappingByPartOverlappingTopLeftBottomRight() {
        Rectangle r1 = new Rectangle(20, 20, 5, 10);
        Rectangle r2 = new Rectangle(15, 18, 6, 10);
        Rectangle overlapr1r2 = new Rectangle(20, 20, 8, 1);
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testFound2RectanglesOverlappingByPartOverlappingTopLeftBottomRightAndMixedCoordinate() {
        Rectangle r1 = new Rectangle(-2, 2, 12, 8);
        Rectangle r2 = new Rectangle(-1, 2, 234, 1024);
        Rectangle overlapr1r2 = new Rectangle(-1, 2, 8, 11);
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }

    @Test
    public void testFound2RectanglesOverlappingByPartOverlappingTopLeftBottomRightAndNegativeCoordinate() {
        Rectangle r1 = new Rectangle(-50, -50, 10, 10);
        Rectangle r2 = new Rectangle(-59, -59, 10, 10);
        Rectangle overlapr1r2 = new Rectangle(-50, -50, 1, 1);
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r1, r2));
        assertEquals(overlapr1r2, RectanglesOverlapping.found2RectanglesOverlapping(r2, r1));
    }
}