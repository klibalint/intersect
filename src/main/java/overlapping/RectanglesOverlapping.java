package overlapping;

import plane_figures.Rectangle;

import java.util.ArrayList;
import java.util.List;

public final class RectanglesOverlapping {

    List<Rectangle> listOfRectangles = new ArrayList<>();

    /**
     * This method checks if there is overlapping between two rectangles.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @return true if there is overlapping, on the other hand it returns false.
     */
    public static boolean is2RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2) {
        Rectangle leftRect;
        Rectangle rightRect;
        Rectangle upperRect;
        Rectangle lowerRect;

        //determine left and right
        if (rectangle1.getxCoordinate() < rectangle2.getxCoordinate()) {
            leftRect = rectangle1;
            rightRect = rectangle2;
        } else {
            leftRect = rectangle2;
            rightRect = rectangle1;
        }

        //determine upper and lower
        if (rectangle1.getyCoordinate() < rectangle2.getyCoordinate()) {
            upperRect = rectangle1;
            lowerRect = rectangle2;
        } else {
            upperRect = rectangle2;
            lowerRect = rectangle1;
        }

        //calculate the overlapW
        if (leftRect.getxCoordinate() + leftRect.getWeight() <= rightRect.getxCoordinate()) {
            return false;
        } else if (upperRect.getyCoordinate() + upperRect.getHeight() <= lowerRect.getyCoordinate()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * This method is using is2RectanglesOverlapping as a helper method. Compares all parameters to all parameter (a
     * parameter is not comparing to itself) and if there is overlapping provided by 3 rectangles it returns true.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @param rectangle3 Rectangle object as a third rectangle parameter.
     * @return true if 3 rectangles have common overlapping area. Else it returns false.
     */
    public static boolean is3RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2, Rectangle rectangle3) {
        return is2RectanglesOverlapping(rectangle1, rectangle2) && is2RectanglesOverlapping(rectangle1, rectangle3) &&
                is2RectanglesOverlapping(rectangle2, rectangle3);
    }

    /**
     * This method is using is2RectanglesOverlapping as a helper method. Compares all parameters to all parameter (a
     * parameter is not comparing to itself) and if there is overlapping provided by 4 rectangles it returns true.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @param rectangle3 Rectangle object as a third rectangle parameter.
     * @param rectangle4 Rectangle object as a fourth rectangle parameter.
     * @return true if 4 rectangles have common overlapping area. Else it returns false.
     */
    public static boolean is4RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2, Rectangle rectangle3,
                                                   Rectangle rectangle4) {
        return is2RectanglesOverlapping(rectangle1, rectangle2) && is2RectanglesOverlapping(rectangle1, rectangle3) &&
                is2RectanglesOverlapping(rectangle1, rectangle4) && is2RectanglesOverlapping(rectangle2, rectangle3) &&
                is2RectanglesOverlapping(rectangle2, rectangle4) && is2RectanglesOverlapping(rectangle3, rectangle4);
    }

    /**
     * This method is using is2RectanglesOverlapping as a helper method. Compares all parameters to all parameter (a
     * parameter is not comparing to itself) and if there is overlapping area provided by 5 rectangles it returns true.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @param rectangle3 Rectangle object as a third rectangle parameter.
     * @param rectangle4 Rectangle object as a fourth rectangle parameter.
     * @param rectangle5 Rectangle object as a fifth rectangle parameter.
     * @return true if 5 rectangles have common overlapping area. Else it returns false.
     */
    public static boolean is5RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2, Rectangle rectangle3,
                                                   Rectangle rectangle4, Rectangle rectangle5) {

        return is2RectanglesOverlapping(rectangle1, rectangle2) && is2RectanglesOverlapping(rectangle1, rectangle3) &&
                is2RectanglesOverlapping(rectangle1, rectangle4) && is2RectanglesOverlapping(rectangle1, rectangle5) &&
                is2RectanglesOverlapping(rectangle2, rectangle3) && is2RectanglesOverlapping(rectangle2, rectangle4) &&
                is2RectanglesOverlapping(rectangle2, rectangle5) && is2RectanglesOverlapping(rectangle3, rectangle4) &&
                is2RectanglesOverlapping(rectangle3, rectangle5) && is2RectanglesOverlapping(rectangle4, rectangle5);
    }

    /**
     * Adds every parameter to a Rectangle type list. Then it uses a double for loop to compare to every element to
     * every element.(same elements are not compared to themselves). If there is on overlapping between to rectangles
     * this method returns false immediately.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @param rectangle3 Rectangle object as a third rectangle parameter.
     * @param rectangle4 Rectangle object as a fourth rectangle parameter.
     * @param rectangle5 Rectangle object as a fifth rectangle parameter.
     * @param rectangle6 Rectangle object as a sixth rectangle parameter.
     * @return true if 6 rectangles have common overlapping area. Else it returns false.
     */
    public static boolean is6RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2, Rectangle rectangle3,
                                                   Rectangle rectangle4, Rectangle rectangle5, Rectangle rectangle6) {

        List<Rectangle> listOfParams = new ArrayList<>();
        listOfParams.add(rectangle1);
        listOfParams.add(rectangle2);
        listOfParams.add(rectangle3);
        listOfParams.add(rectangle4);
        listOfParams.add(rectangle5);
        listOfParams.add(rectangle6);

        for (int i = 0; i < listOfParams.size() - 1; i++) {
            for (int j = i + 1; j < listOfParams.size(); j++) {
                if (!is2RectanglesOverlapping(listOfParams.get(i), listOfParams.get(j))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Adds every parameter to a Rectangle type list. Then it uses a double for loop to compare to every element to
     * every element.(same elements are not compared to themselves). If there is on overlapping between to rectangles
     * this method returns false immediately.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @param rectangle3 Rectangle object as a third rectangle parameter.
     * @param rectangle4 Rectangle object as a fourth rectangle parameter.
     * @param rectangle5 Rectangle object as a fifth rectangle parameter.
     * @param rectangle6 Rectangle object as a sixth rectangle parameter.
     * @param rectangle7 Rectangle object as a seventh rectangle parameter.
     * @return true if 7 rectangles have common overlapping area. Else it returns false.
     */
    public static boolean is7RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2, Rectangle rectangle3,
                                                   Rectangle rectangle4, Rectangle rectangle5, Rectangle rectangle6,
                                                   Rectangle rectangle7) {

        List<Rectangle> listOfParams = new ArrayList<>();
        listOfParams.add(rectangle1);
        listOfParams.add(rectangle2);
        listOfParams.add(rectangle3);
        listOfParams.add(rectangle4);
        listOfParams.add(rectangle5);
        listOfParams.add(rectangle6);
        listOfParams.add(rectangle7);

        for (int i = 0; i < listOfParams.size() - 1; i++) {
            for (int j = i + 1; j < listOfParams.size(); j++) {
                if (!is2RectanglesOverlapping(listOfParams.get(i), listOfParams.get(j))) {
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * Adds every parameter to a Rectangle type list. Then it uses a double for loop to compare to every element to
     * every element.(same elements are not compared to themselves). If there is on overlapping between to rectangles
     * this method returns false immediately.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @param rectangle3 Rectangle object as a third rectangle parameter.
     * @param rectangle4 Rectangle object as a fourth rectangle parameter.
     * @param rectangle5 Rectangle object as a fifth rectangle parameter.
     * @param rectangle6 Rectangle object as a sixth rectangle parameter.
     * @param rectangle7 Rectangle object as a seventh rectangle parameter.
     * @param rectangle8 Rectangle object as a eighth rectangle parameter.
     * @return true if 8 rectangles have common overlapping area. Else it returns false.
     */
    public static boolean is8RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2, Rectangle rectangle3,
                                                   Rectangle rectangle4, Rectangle rectangle5, Rectangle rectangle6,
                                                   Rectangle rectangle7, Rectangle rectangle8) {

        List<Rectangle> listOfParams = new ArrayList<>();
        listOfParams.add(rectangle1);
        listOfParams.add(rectangle2);
        listOfParams.add(rectangle3);
        listOfParams.add(rectangle4);
        listOfParams.add(rectangle5);
        listOfParams.add(rectangle6);
        listOfParams.add(rectangle7);
        listOfParams.add(rectangle8);

        for (int i = 0; i < listOfParams.size() - 1; i++) {
            for (int j = i + 1; j < listOfParams.size(); j++) {
                if (!is2RectanglesOverlapping(listOfParams.get(i), listOfParams.get(j))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Adds every parameter to a Rectangle type list. Then it uses a double for loop to compare to every element to
     * every element.(same elements are not compared to themselves). If there is on overlapping between to rectangles
     * this method returns false immediately.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @param rectangle3 Rectangle object as a third rectangle parameter.
     * @param rectangle4 Rectangle object as a fourth rectangle parameter.
     * @param rectangle5 Rectangle object as a fifth rectangle parameter.
     * @param rectangle6 Rectangle object as a sixth rectangle parameter.
     * @param rectangle7 Rectangle object as a seventh rectangle parameter.
     * @param rectangle8 Rectangle object as a eighth rectangle parameter.
     * @param rectangle9 Rectangle object as a ninth rectangle parameter.
     * @return true if 9 rectangles have common overlapping area. Else it returns false.
     */
    public static boolean is9RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2, Rectangle rectangle3,
                                                   Rectangle rectangle4, Rectangle rectangle5, Rectangle rectangle6,
                                                   Rectangle rectangle7, Rectangle rectangle8, Rectangle rectangle9) {

        List<Rectangle> listOfParams = new ArrayList<>();
        listOfParams.add(rectangle1);
        listOfParams.add(rectangle2);
        listOfParams.add(rectangle3);
        listOfParams.add(rectangle4);
        listOfParams.add(rectangle5);
        listOfParams.add(rectangle6);
        listOfParams.add(rectangle7);
        listOfParams.add(rectangle8);
        listOfParams.add(rectangle9);

        for (int i = 0; i < listOfParams.size() - 1; i++) {
            for (int j = i + 1; j < listOfParams.size(); j++) {
                if (!is2RectanglesOverlapping(listOfParams.get(i), listOfParams.get(j))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Adds every parameter to a Rectangle type list. Then it uses a double for loop to compare to every element to
     * every element.(same elements are not compared to themselves). If there is on overlapping between to rectangles
     * this method returns false immediately.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @param rectangle3 Rectangle object as a third rectangle parameter.
     * @param rectangle4 Rectangle object as a fourth rectangle parameter.
     * @param rectangle5 Rectangle object as a fifth rectangle parameter.
     * @param rectangle6 Rectangle object as a sixth rectangle parameter.
     * @param rectangle7 Rectangle object as a seventh rectangle parameter.
     * @param rectangle8 Rectangle object as a eighth rectangle parameter.
     * @param rectangle9 Rectangle object as a ninth rectangle parameter.
     * @param rectangle10 Rectangle object as a tenth rectangle parameter.
     * @return true if 10 rectangles have common overlapping area. Else it returns false.
     */
    public static boolean is10RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2, Rectangle rectangle3,
                                                   Rectangle rectangle4, Rectangle rectangle5, Rectangle rectangle6,
                                                   Rectangle rectangle7, Rectangle rectangle8, Rectangle rectangle9,
                                                   Rectangle rectangle10) {

        List<Rectangle> listOfParams = new ArrayList<>();
        listOfParams.add(rectangle1);
        listOfParams.add(rectangle2);
        listOfParams.add(rectangle3);
        listOfParams.add(rectangle4);
        listOfParams.add(rectangle5);
        listOfParams.add(rectangle6);
        listOfParams.add(rectangle7);
        listOfParams.add(rectangle8);
        listOfParams.add(rectangle9);
        listOfParams.add(rectangle10);

        for (int i = 0; i < listOfParams.size() - 1; i++) {
            for (int j = i + 1; j < listOfParams.size(); j++) {
                if (!is2RectanglesOverlapping(listOfParams.get(i), listOfParams.get(j))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * When that is sure there is overlapping between two rectangles, this method only should be used. Waits two
     * rectangles and return back a third one. This returned object contains the coordinate, height and weight of the
     * overlapping volume.
     *
     * @param rectangle1 Rectangle object as a first rectangle parameter.
     * @param rectangle2 Rectangle object as a second rectangle parameter.
     * @return Rectangle object what is created by the overlapping.
     */
    public static Rectangle found2RectanglesOverlapping(Rectangle rectangle1, Rectangle rectangle2) {
        int overlapX;
        int overlapY;
        int overlapH;
        int overlapW;

        Rectangle leftRect;
        Rectangle rightRect;
        Rectangle upperRect;
        Rectangle lowerRect;

        //determine left and right
        if (rectangle1.getxCoordinate() < rectangle2.getxCoordinate()) {
            leftRect = rectangle1;
            rightRect = rectangle2;
        } else {
            leftRect = rectangle2;
            rightRect = rectangle1;
        }

        //determine upper and lower
        if (rectangle1.getyCoordinate() < rectangle2.getyCoordinate()) {
            upperRect = rectangle1;
            lowerRect = rectangle2;
        } else {
            upperRect = rectangle2;
            lowerRect = rectangle1;
        }

        //define the coordinates, heights, and weights of the overlapping rectangle
        overlapX = rightRect.getxCoordinate();
        overlapY = lowerRect.getyCoordinate();

        if (upperRect.getyCoordinate() + upperRect.getHeight() >= lowerRect.getyCoordinate() + lowerRect.getHeight()) {
            //full overlap
            overlapH = lowerRect.getHeight();
        } else {
            //partial overlap
            overlapH = upperRect.getyCoordinate() + upperRect.getHeight() - lowerRect.getyCoordinate();
        }

        if (leftRect.getxCoordinate() + leftRect.getWeight() >= rightRect.getxCoordinate() + rightRect.getWeight()) {
            //full overlap
            overlapW = rightRect.getWeight();
        } else {
            //partial overlap
            overlapW = leftRect.getxCoordinate() + leftRect.getWeight() - rightRect.getxCoordinate();
        }

        return new Rectangle(overlapX, overlapY, overlapH, overlapW);
    }
}
