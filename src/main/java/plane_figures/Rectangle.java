package plane_figures;

import java.util.Objects;

public class Rectangle {

    private int xCoordinate;
    private int yCoordinate;
    private int weight;
    private int height;


    //constructor
    public Rectangle(int xCoordinate, int yCoordinate, int weight, int height) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.weight = weight;
        this.height = height;
    }

    //getters
    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    //toString
    @Override
    public String toString() {
        return "Rectangle{" +
                "xCoordinate=" + xCoordinate +
                ", yCoordinate=" + yCoordinate +
                ", weight=" + weight +
                ", height=" + height +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Rectangle) {
            return this.getxCoordinate() == (((Rectangle) o).getxCoordinate()) &&
                    this.getyCoordinate() == (((Rectangle) o).getyCoordinate()) &&
                    this.getHeight() == (((Rectangle) o).getHeight()) &&
                    this.getWeight() == (((Rectangle) o).getWeight());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(getxCoordinate(), getyCoordinate(), getWeight(), getHeight());
    }
}
