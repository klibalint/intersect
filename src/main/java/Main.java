import overlapping.RectanglesOverlapping;
import plane_figures.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        String fullPath = "/home/superkliba/DEV/rectanglesintersect/src/testJSON/JSONfile1";

        //System.out.println(Integer.MAX_VALUE);
        //JSONReader.readJSON(fullPath);

        Rectangle r5 = new Rectangle(5, 5, 5, 5);
        Rectangle r6 = new Rectangle(9, 9, 5, 5);
        System.out.println(RectanglesOverlapping.found2RectanglesOverlapping(r5, r6));
        List<Rectangle> testList = new ArrayList<>();
        testList.add(r5);
        testList.add(r5);
        testList.add(r6);

        for (int i = 0; i < testList.size() - 1; i++){
            for (int j = i + 1; j < testList.size(); j++) {
                Rectangle temp = RectanglesOverlapping.found2RectanglesOverlapping(testList.get(i), testList.get(j));

                System.out.println("Between rectangle " + (i + 1) + " and " + (j + 1) + " at (" +
                        temp.getxCoordinate() + "," + temp.getyCoordinate() + "), w=" + temp.getWeight() +
                        ", h=" + temp.getHeight() + ".");
            }
        }
    }
}
