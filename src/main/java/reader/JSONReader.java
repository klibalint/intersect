package reader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import plane_figures.Rectangle;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public final class JSONReader {

    private static List<Rectangle> listOfRectangles = new ArrayList<>();

    public static List<Rectangle> getListOfRectangles() {
        return listOfRectangles;
    }

    //JSON reader method
    public static List<Rectangle> readJSON(String fullPath) {
        JSONParser parser = new JSONParser();
        try {
            Object object = parser
                    .parse(new FileReader(fullPath));

            //convert Object to JSONObject
            JSONObject jsonObject = (JSONObject) object;

            //Reading the array
            JSONArray rectangles = (JSONArray) jsonObject.get("rects");

            //Printing all the values
            System.out.println("rectangles:");

            for (Object rect : rectangles) {
                if (rect instanceof JSONObject) {
                    JSONObject rectJson = (JSONObject) rect;
                    try {
                        int x = JSONReader.castJSONToInteger(rectJson.get("x"));
                        int y = JSONReader.castJSONToInteger(rectJson.get("y"));
                        int h = JSONReader.castJSONToInteger(rectJson.get("h"));
                        int w = JSONReader.castJSONToInteger(rectJson.get("w"));
                        //                 System.out.print(rectJson.get("h") + " ");
                        //                System.out.print(rectJson.get("w") + " ");

                        if (isRectangleAndCoordinateValid(x, y, h, w))
                            listOfRectangles.add(new Rectangle(x, y, h, w));


                        if (isListEnoughLong(listOfRectangles)) {
                            System.out.println(listOfRectangles.size());
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                System.out.println(listOfRectangles);
                System.out.println(listOfRectangles.size());

            }

        } catch (Exception fe) {
            fe.printStackTrace();
        }

        return listOfRectangles;
    }

    /**
     * Waits an object and tries to cast to Integer.
     * @param object object that should be an Integer object
     * @return If it can be an Integer we get back the param as an Integer. On the other hand it throws a
     * NumberFormatException.
     */
    private static Integer castJSONToInteger(Object object) {
        Integer output;

        try {
            output = Integer.parseInt(object + "");
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return null;
        }

        return output;
    }

    private static boolean isListEnoughLong(List<Rectangle> listOfRectangles) {
        return listOfRectangles.size() >= 11;
    }

    /**
     * We have a coordinate table and max 10 rectangles in one board. As the specification describes the coordinate and
     * height and weight will be added Integer data type. This method calculates if these params are not fit to Integer
     * value.
     * @param x x coordinate point parameter as an integer
     * @param y y coordinate point parameter as an integer
     * @param h height of the rectangle as an integer
     * @param w weight of the rectangle as an integer
     * @return if it is a valid rectangle and can be placed to the coordinate table it is true. On the other hand it
     * throws an IllegalArgumentException with an error message.
     */
    private static boolean isRectangleAndCoordinateValid(int x, int y, int h, int w) {
        Double xd = (double) x;
        Double yd = (double) y;
        Double hd = (double) h;
        Double wd = (double) w;

        if (xd + wd > Integer.MAX_VALUE || yd + hd > Integer.MAX_VALUE ||
                wd < 1 || hd < 1) {
            throw new IllegalArgumentException("Rectangle is not fit to an integer coordinate table or weight and/or" +
                    "height is not positive number.");
        }

        return true;
    }
}
